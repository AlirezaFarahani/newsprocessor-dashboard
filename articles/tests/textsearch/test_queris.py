from django.test import TestCase

from articles.models import Association
from articles.operations import advanced_article_search
from articles.tests.utils import sample_article, sample_article2, sample_article3


class TestArticleAdvanceSearch(TestCase):
    def setUp(self) -> None:
        self.nature_journal = Association.objects.create(name="Nature", type=Association.AssociationType.Journal)
        self.tokyo_uni = Association.objects.create(name="University of Tokyo",
                                                    type=Association.AssociationType.Institution)
        self.umass_fund = Association.objects.create(name="University of Massachusetts Amherst Armstrong Fund",
                                                     type=Association.AssociationType.Funder)
        self.articles = (sample_article(), sample_article2(), sample_article3())
        for article in self.articles:
            article.save()

        self.articles[0].journal = self.nature_journal
        self.articles[0].institution = self.tokyo_uni
        self.articles[1].institution = self.tokyo_uni
        self.articles[1].funder = self.umass_fund

        for article in self.articles:
            article.save()

    def tearDown(self) -> None:
        pass

    def test_single_condition(self):
        qs = advanced_article_search(journal=self.nature_journal)
        self.assertEqual(len(qs), 1)
        self.assertEqual(qs[0], self.articles[0])

        qs = advanced_article_search(institution=self.tokyo_uni)
        self.assertEqual(len(qs), 2)
        self.assertEqual({*qs}, {self.articles[0], self.articles[1]})

    def test_articles_must_match_with_all_fields(self):
        qs = advanced_article_search(institution=self.tokyo_uni, funder=self.umass_fund)
        self.assertEqual(len(qs), 1)
        self.assertEqual(qs[0], self.articles[1])

    # def test_single_absence_returns_empty_queryset(self):
    #     qs = Article.objects.raw(build_textsearch_queryset(journal="-1"))
    #     self.assertEqual(len(qs), 0)
    #
    #     qs = Article.objects.raw(build_textsearch_queryset(journal="-1", funder=self.umass_fund))
    #     self.assertEqual(len(qs), 0)

    def test_textsearch_with_sql_search(self):
        qs = advanced_article_search(q="gold", funder=self.umass_fund)
        self.assertEqual(len(qs), 0)

        qs = advanced_article_search(q="pollution", funder=self.umass_fund)
        self.assertEqual(len(qs), 1)

        self.articles[0].content += " jjj"
        self.articles[1].title += " jjj"
        self.articles[0].save()
        self.articles[1].save()

        qs = advanced_article_search(q="jjj", institution=self.tokyo_uni)
        self.assertEqual({*qs}, {self.articles[1], self.articles[0]})
