import datetime

from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db import IntegrityError
from django.test import TestCase

# Create your tests here.
from articles.models import Tag, Article, Comment
from articles.tests.utils import sample_article, sample_article2


# noinspection DuplicatedCode
class ArticleTest(TestCase):
    # @classmethod
    # def setUpTestData(cls):
    #     Article.objects.

    def setUp(self):
        self.article = sample_article()
        self.assertIsNone(self.article.id)

    def test_valid_article(self):
        self.article.full_clean()
        self.article.save()
        self.assertIsNotNone(self.article.id)
        self.assertEqual(self.article.url, "https://www.eurekalert.org/pub_releases/2021-06/dnl-sga062421.php")

    def test_subtitle_is_optional(self):
        self.article.subtitle = None
        self.article.full_clean()
        self.article.save()
        self.assertIsNotNone(self.article.id)

    def test_keyword_is_optional(self):
        self.article.keywords = None
        self.article.full_clean()
        self.article.save()
        self.assertIsNotNone(self.article.id)

    def test_publish_date_is_not_future(self):
        self.article.publish_date = datetime.date.today() + datetime.timedelta(days=1)
        with self.assertRaises(IntegrityError):
            self.article.save()
        # with self.assertRaises(ValidationError) as e_context_manager:
        #     self.article.full_clean()
        #     self.article.save()
        # ve = e_context_manager.exception
        # self.assertIn('publish_date', ve.error_dict)

    def test_keywords_must_be_json_array(self):
        self.article.keywords = {"keyword", "KEY"}
        with self.assertRaises(ValidationError) as e_context_manager:
            self.article.full_clean()
            self.article.save()
        ve = e_context_manager.exception
        self.assertIn('keywords', ve.error_dict)

    # def test_keywords_must_not_be_empty(self):
    #     self.article.keywords = []
    #     with self.assertRaises(ValidationError) as e_context_manager:
    #         self.article.full_clean()
    #         self.article.save()
    #     ve = e_context_manager.exception
    #     self.assertIn('keywords', ve.error_dict)

    def test_articles_of_following_tags_query(self):
        user1 = User.objects.create_user(username="ali", email="a@b.com")
        user2 = User.objects.create_user(username="gholi", email="a@d.com")
        for i in range(1, 6):
            if i < 3:
                t = Tag.objects.create(title=f"Quantum {i}", creator=user1)
                t.followers.add(user1)
            else:
                t = Tag.objects.create(title=f"Quantum {i}", creator=user2)
                t.followers.add(user2)

        article1 = sample_article()
        article2 = sample_article2()
        for a in (article1, article2):
            a.save()
        article3 = sample_article2()
        article3.title = "hehe"
        article3.url = "https://google.com"
        article3.save()
        article1.tags.add(Tag.objects.get(pk=1), Tag.objects.get(pk=3), Tag.objects.get(pk=5))
        article2.tags.add(Tag.objects.get(pk=2), Tag.objects.get(pk=4))
        article3.tags.add(Tag.objects.get(pk=4))
        # user1 follows tag 1,2,3. so he's interested in article 1,2
        # user2 follows tag 4,5. so he's interested in article 1,2,3
        self.assertEqual(Article.objects.filter(tags__followers=user1).distinct().count(), 2)
        self.assertEqual(Article.objects.filter(tags__followers=user2).distinct().count(), 3)

    def test_only_one_comment_per_user(self):
        user = User.objects.create_user(username="ali", email="a@b.com")
        user2 = User.objects.create_user(username="gholi", email="a@d.com")
        article = sample_article()
        article.save()
        Comment(author=user, content="Excellent article", article=article).save()
        Comment(author=user2, content="Terrible article!", article=article).save()
        with self.assertRaises(IntegrityError):
            Comment(author=user, content="Yeah, it's terrible!", article=article).save()

    def test_commented_articles_query(self):
        user1 = User.objects.create_user(username="ali", email="a@b.com")
        user2 = User.objects.create_user(username="gholi", email="a@d.com")

        article1 = sample_article()
        article2 = sample_article2()
        for a in (article1, article2):
            a.save()
        article3 = sample_article2()
        article3.title = "hehe"
        article3.url = "https://google.com"
        article3.save()

        Comment.objects.create(author=user1, content="Excellent article", article=article1)
        Comment.objects.create(author=user2, content="Terrible article!", article=article1)
        Comment.objects.create(author=user2, content="Such nano!", article=article2)
        self.assertEqual(Article.objects.filter(comments__author=user1).count(), 1)
        self.assertEqual(Article.objects.filter(comments__author=user2).count(), 2)
