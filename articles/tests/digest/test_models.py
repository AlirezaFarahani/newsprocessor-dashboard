from datetime import date, timedelta

from django.contrib.auth.models import User
from django.db import IntegrityError
from django.test import TestCase

from articles.models import Digest


class DigestTest(TestCase):
    def setUp(self) -> None:
        self.user = User.objects.create_user(username='jacob', email='jacob@…')

    def test_creation_date_could_not_be_future(self):
        digest = Digest(recipient=self.user, creation_date=date.today() + timedelta(1))
        with self.assertRaises(IntegrityError):
            digest.save()
