from django.conf.urls.static import static
from django.urls import path, include

from articles.views import (AddOrRemoveArticleTagView, ArticleTagSearchView, AllArticlesListView, ArticlesByTagListView,
                            ArticlesByAssociationListView, ArticleCommentsListView, CommentCreateView,
                            CommentDetailView, ArticleDetailView, ArticleTagsListView, create_tag_and_add_to_article,
                            AssociationAutocompleteView, SearchableAssociationsListView, CommentUpdateView,
                            CommentDeleteView, DigestListView, DigestDetailView, ReportsTabView, TagTrendReportView,
                            MostUsedTagsReportView, CSVReportView, TagAutocompleteView, AllTagsListView,
                            CreatedByMeTagsListView, TagCoOccurrenceReportView, VisualWordCloudReportView,
                            TextualWordCloudReportView, TagCreateView, ToggleTagFollowershipView,
                            FollowedByMeTagsListView, TextSearchArticleListView, FollowedArticlesListView,
                            CommentedArticlesListView, AssociationProfileView, AssociationProfileCreateView,
                            AssociationProfileUpdateView, MetaTagsListView, CommentedByMeArticlesListView)
from newsprocessor_dashboard import settings

app_name = 'articles'

# ******** ATTENTION! *********
# There are few cases we hard coded urls.
# 1. Wherever Using HTMX path-deps.
#       (currently tag lists and article tags, so careful with 'tags/*' and 'articles/<id>/tags/*'
# 2. Sending request to add tag to article. -> 'articles/<id>/tags/<id>'

urlpatterns = [
    # region Tags
    path('tags/', include([
        # Queries
        path('', AllTagsListView.as_view(), name='tags-all'),
        path('created/', CreatedByMeTagsListView.as_view(), name='tags-created-by-me'),
        path('followed/', FollowedByMeTagsListView.as_view(), name='tags-followed-by-me'),
        path('meta/', MetaTagsListView.as_view(), name='tags-meta'),
        path('search/autocomplete/', TagAutocompleteView.as_view(), name='tags-autocomplete'),

        # Single tag
        # path('<int:pk>/', TagDetailView.as_view(), name='tag-detail'),
        path('create/', TagCreateView.as_view(), name='tag-create'),
        # path('<int:pk>/update/', TagUpdateView.as_view(), name='tag-update'),
        # path('<int:pk>/delete/', TagDeleteView.as_view(), name='tag-delete'),
        path('<int:pk>/followership/', ToggleTagFollowershipView.as_view(), name='tag-toggle-followership'),
    ])),
    # endregion

    # region Articles
    path('articles/', include([
        # Queries
        path('', AllArticlesListView.as_view(), name='articles-all'),
        path('interested/', FollowedArticlesListView.as_view(), name='articles-by-followed-tags'),
        path('commented/', CommentedArticlesListView.as_view(), name='articles-commented'),
        path('commented-by-me/', CommentedByMeArticlesListView.as_view(), name='articles-commented-by-me'),
        path('by-tag/<int:tag_id>/', ArticlesByTagListView.as_view(), name='articles-by-tag'),
        path('by-association/<int:association_id>/', ArticlesByAssociationListView.as_view(),
             name='articles-by-association'),
        path('search/', TextSearchArticleListView.as_view(), name='articles-text-search'),

        # Single article details
        path('<int:pk>/', ArticleDetailView.as_view(), name='article-detail'),

        path('<int:article_id>/', include([
            # Article tags
            path('tags/', ArticleTagsListView.as_view(), name='article-tags'),
            path('tags/create-and-add/', create_tag_and_add_to_article, name='article-tag-create-and-add'),
            path('tags/<int:tag_id>/', AddOrRemoveArticleTagView.as_view(), name='article-tag-addOrRemove'),
            path('tags/search/', ArticleTagSearchView.as_view(), name='article-tag-search'),

            # Article comment
            path('comments/', ArticleCommentsListView.as_view(), name='article-comments'),
            path('user_comment/', CommentDetailView.as_view(), name='article-usercomment-details'),
            path('comment/create/', CommentCreateView.as_view(), name='article-comment-create'),
            path('user_comment/update/', CommentUpdateView.as_view(), name='article-usercomment-update'),
            path('user_comment/delete/', CommentDeleteView.as_view(), name='article-usercomment-delete'),
        ]))
    ])),
    # endregion

    path('reports/', include([
        path('', ReportsTabView.as_view(), name='tabs-reports'),
        path('tag-trend/', TagTrendReportView.as_view(), name='reports-tag-trend'),
        path('most-used-tags/', MostUsedTagsReportView.as_view(), name='reports-most-used-tags'),
        path('tag-co-occurrence/', TagCoOccurrenceReportView.as_view(), name='reports-tag-co-occurrence'),
        path('visual-wordcloud/', VisualWordCloudReportView.as_view(), name='reports-visual-wordcloud'),
        path('textual-wordcloud/', TextualWordCloudReportView.as_view(), name='reports-textual-wordcloud'),
        path('csv/', CSVReportView.as_view(), name='reports-csv'),
    ])),

    path('digests/', include([
        path('', DigestListView.as_view(), name='digests-list'),
        path('<int:pk>/', DigestDetailView.as_view(), name='digest-detail')
    ])),

    path('assocs/', include([
        path('<slug:association_type>/', SearchableAssociationsListView.as_view(), name='association-list'),
        path('<slug:association_type>/search/', AssociationAutocompleteView.as_view(), name='assoc-autocomplete'),
        path('<int:pk>/profile/', AssociationProfileView.as_view(), name='association-profile'),
        path('<int:pk>/profile/create/', AssociationProfileCreateView.as_view(),
             name='association-profile-create'),
        path('<int:pk>/profile/update/', AssociationProfileUpdateView.as_view(),
             name='association-profile-update'),
    ])),
]

# https://testdriven.io/blog/django-static-files/
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
