from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db import IntegrityError
from django.test import TestCase

from articles.models import Tag
from articles.validators import non_empty_english_string


class TagTest(TestCase):
    def test_model_str(self):
        tag = Tag(title="Nano Science", creator=User(username="alialavi", email="ali.alavi@example.com"))
        self.assertEqual(str(tag), "Nano Science")

    def test_tag_creator_null_after_creator_delete(self):
        user = User.objects.create_user(username="taghitaghavi", email="taghi.taghavi@example.com")
        # with self.assertRaises(IndentationError):
        tag = Tag.objects.create(title="Quantum physics", creator=user)
        user.delete()
        tag.refresh_from_db()
        self.assertIsNone(tag.creator)

    def test_tags_default_ordering_by_title(self):
        user1 = User.objects.create_user(username="taghitaghavi", email="taghi.taghavi@example.com")
        user2 = User.objects.create_user(username="alialavi", email="ali.alavi@example.com")
        Tag.objects.create(title="B", creator=user1)
        Tag.objects.create(title="A", creator=user2)
        self.assertListEqual(["A", "B"], list(Tag.objects.values_list('title', flat=True)))

    def test_teg_deletion_remove_it_from_followers(self):
        user1 = User.objects.create_user(username="taghitaghavi", email="taghi.taghavi@example.com")
        user2 = User.objects.create_user(username="alialavi", email="ali.alavi@example.com")
        tag = Tag.objects.create(title='Quantum', creator=user1)
        tag.followers.add(user1, user2)
        for user in (user1, user2):
            self.assertEqual(1, user.following_tags.count())
        tag.delete()

        for user in (user1, user2):
            self.assertEqual(0, user.following_tags.count())

    def test_tags_must_be_case_insensitive_unique(self):
        user1 = User.objects.create_user(username="taghitaghavi", email="taghi.taghavi@example.com")
        tag = Tag.objects.create(title='Quantum', creator=user1)
        self.assertEqual(tag.title, 'Quantum')
        with self.assertRaises(IntegrityError) as ie:
            Tag.objects.create(title='quantum', creator=user1)
        self.assertIn("case insensitive", str(ie.exception))


class CustomValidatorsTest(TestCase):
    def test_invalid_tag_titles(self):
        validator = non_empty_english_string
        for title in ["", " hello ", "@@", "دفاع", "social-network", "social_network", "social  network"]:
            with self.assertRaises(ValidationError) as cm:
                validator(title)

    def test_valid_tag_titles(self):
        validator = non_empty_english_string
        for title in ["Nano", "nano", "Nano Technology", "123", "Nano123"]:
            try:
                validator(title)
            except ValidationError as ve:
                self.fail(ve)
