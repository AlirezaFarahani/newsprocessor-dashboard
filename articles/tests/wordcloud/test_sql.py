from django.db import connection
from django.test import TestCase

from articles.tests.utils import sample_article, sample_article2


class WordCloudTest(TestCase):
    def setUp(self) -> None:
        article = sample_article()
        article.title = "A sample title"
        article.Content = "Iran Iran Football Football. <Hello>Iran</Hello>."
        article.save()
        article2 = sample_article2()
        article2.title = "irrelevant"
        article2.content = "payandeh bashi Iran"
        article2.save()

    def test_postgres_sql_query_works(self):
        with connection.cursor() as cursor:
            cursor.execute(f"""
            SELECT * FROM ts_stat('SELECT vector_column FROM articles_article')
            ORDER BY nentry DESC, ndoc DESC, word
        """)
            word_cloud = cursor.fetchall()
        # word_cloud should be like: [('<word>', document_freq, total_freq)]
        self.assertIsInstance(word_cloud, list)
        self.assertIsInstance(word_cloud[0], tuple)
        self.assertEqual(len(word_cloud[0]), 3)
