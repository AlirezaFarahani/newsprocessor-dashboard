from django.contrib.auth.models import User
from django.db import IntegrityError
from django.test import TestCase

from articles.models import Tag, ArticleTag
from articles.tests.utils import sample_article


class TestArticleTag(TestCase):

    def setUp(self):
        self.user = User.objects.create_user(username="jacob", email="a@b.com")
        self.tag = Tag.objects.create(title="Quantum", creator=self.user)
        self.article = sample_article()
        self.article.save()

    def test_tags_are_unique_per_article(self):
        ArticleTag.objects.create(article=self.article, tag=self.tag, added_by=self.user)
        with self.assertRaises(IntegrityError):
            ArticleTag.objects.create(article=self.article, tag=self.tag, added_by=self.user)
