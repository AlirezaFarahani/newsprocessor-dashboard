from crispy_forms.helper import FormHelper
from crispy_forms.layout import Field
from dal import autocomplete
from django import forms
from django.contrib.auth.forms import AuthenticationForm, PasswordChangeForm
from django.core.exceptions import ValidationError
from django.db import IntegrityError
from django.db.models import TextChoices
from django.forms import TextInput
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _

from articles.models import Comment, Tag, Association, AssociationProfile


# Solely used for Tag creating in Article Details
class TagModelForm(forms.ModelForm):
    def save(self, commit=True):  # Due to strange django-crispy bug when a field is named 'tag'!
        try:
            return super().save(commit)
        except IntegrityError as e:
            if 'unique case insensitive title' in str(e):
                self.add_error('title', ValidationError(
                    _('%(model_name)s with this %(field_label)s already exists.')
                    % {'model_name': _('Tag'), 'field_label': _('Title')},
                    code="unique"))
                return super().save(commit)
            else:
                raise e

    class Meta:
        model = Tag
        fields = ['title', 'is_meta']


class TagCreationForm(TagModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper['title'].wrap(Field, autofocus="")
        self.helper['is_meta'].wrap(Field, wrapper_class="form-check")
        self.helper.form_tag = False
        self.helper.disable_csrf = True  # no need, request is sent via htmx

    class Meta(TagModelForm.Meta):
        help_texts = {
            'title': _("English characters and numbers")
        }


class CommentModelForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # If you pass FormHelper constructor a form instance
        # It builds a default layout with all its fields
        self.helper = FormHelper(self)
        # self.helper['content'].update_attributes(autofocus=False)
        self.helper['content'].wrap(Field,
                                    autofocus="",  # to not having autofocus, we MUST not put in the tag
                                    placeholder=_("Maximum 500 characters"))
        self.helper.form_show_labels = False
        self.helper.form_tag = False
        # in raw manual rendering, csrf and input is separate from {{ from }}. Just to be in sync with that.
        self.helper.disable_csrf = True
        # You can dynamically adjust your layout
        # self.helper.layout.append(Submit('save', 'save'))

    class Meta:
        model = Comment
        fields = ['content']


class _BaseAssociationForm(forms.Form):
    journal = forms.ModelChoiceField(
        label=_('Journal'),
        queryset=Association.journals.all(),
        # queryset=Association.objects.none(),
        required=False,
        widget=autocomplete.ModelSelect2(
            url=reverse_lazy('articles:assoc-autocomplete', kwargs={
                'association_type': 'Journal'
            }),
            forward=['institution', 'funder'],
            attrs={'data-minimum-input-length': 0, 'data-theme': 'bootstrap-5', 'data-allow-clear': True}
        )
    )
    institution = forms.ModelChoiceField(
        label=_('Institution'),
        queryset=Association.institutions.all(),
        # queryset=Association.objects.none(),
        required=False,
        widget=autocomplete.ModelSelect2(
            url=reverse_lazy('articles:assoc-autocomplete', kwargs={
                'association_type': 'Institution'
            }),
            forward=['journal', 'funder'],
            attrs={'data-placeholder': _('University, college, etc'),
                   'data-minimum-input-length': 0, 'data-theme': 'bootstrap-5', 'data-allow-clear': True}
        )
    )
    funder = forms.ModelChoiceField(
        label=_('Funder'),
        queryset=Association.funders.all(),
        # queryset=Association.objects.all(),
        required=False,
        widget=autocomplete.ModelSelect2(
            url=reverse_lazy('articles:assoc-autocomplete', kwargs={
                'association_type': 'Funder'
            }),
            forward=['journal', 'institution'],
            attrs={'data-minimum-input-length': 0, 'data-theme': 'bootstrap-5', 'data-allow-clear': True}
        )
    )

    field_order = ['journal', 'institution', 'funder']


class TextSearchForm(_BaseAssociationForm):
    long_help_text = _("""
nano tech matches with news having both nano and technology words.
nano OR tech matches with news having any of nano or technology words.
"nano tech" only matches news having exact this phrase.
nano -tech matches with news having nano word but not tech.
""")
    field_order = ['q'] + _BaseAssociationForm.field_order + ['include_comments']

    q = forms.CharField(max_length=100, required=False,
                        help_text=_("Search in titles and contents with web-search operators"),
                        widget=TextInput(attrs={'long_help_text': long_help_text}))

    include_comments = forms.BooleanField(label=_('Include comments in search'),
                                          required=False, initial=False)

    def clean(self):
        cleaned_data = super().clean()
        if not any([v for k, v in cleaned_data.items() if k != 'include_comments']):
            raise ValidationError(_("Form cannot be empty."), code="Empty Form")


class Period(TextChoices):
    LAST_WEEK = 'W', _("Last Week")
    LAST_MONTH = 'M', _("Last Month")
    LAST_YEAR = 'Y', _("Last Year")
    ALL_TIME = 'A', _('All Time')


class TagTrendReportForm(_BaseAssociationForm):
    prefix = "tag_trend"
    field_order = ['tag', 'period'] + _BaseAssociationForm.field_order

    period = forms.ChoiceField(label=_('Period'), required=True, choices=Period.choices, initial=Period.LAST_YEAR)

    tag = forms.ModelChoiceField(
        label=_('Tag'),
        queryset=Tag.objects.all(),
        # queryset=Association.objects.none(),
        required=True,
        widget=autocomplete.ModelSelect2(
            url=reverse_lazy('articles:tags-autocomplete'),
            attrs={'data-minimum-input-length': 2, 'data-theme': 'bootstrap-5', 'data-placeholder': _('Search')}
        )
    )


class MostUsedTagsReportForm(_BaseAssociationForm):
    prefix = "most_used"
    field_order = ['period'] + _BaseAssociationForm.field_order

    period = forms.ChoiceField(label=_('Period'), required=True, choices=Period.choices, initial=Period.LAST_YEAR)


class TagCoOccurrenceReportForm(_BaseAssociationForm):
    prefix = "co_oc"
    field_order = ['tag', 'period'] + _BaseAssociationForm.field_order

    period = forms.ChoiceField(label=_('Period'), required=False, choices=Period.choices, initial=Period.ALL_TIME)

    tag = forms.ModelChoiceField(
        label=_('Tag'),
        queryset=Tag.objects.all(),
        required=True,
        widget=autocomplete.ModelSelect2(
            url=reverse_lazy('articles:tags-autocomplete'),
            attrs={'data-minimum-input-length': 2, 'data-theme': 'bootstrap-5', 'data-placeholder': _('Search')}
        )
    )


class WordCloudReportForm(_BaseAssociationForm):
    prefix = "textual_wc"
    field_order = ['tags', 'period'] + _BaseAssociationForm.field_order

    period = forms.ChoiceField(label=_('Period'), required=True, choices=Period.choices, initial=Period.LAST_YEAR)

    tags = forms.ModelMultipleChoiceField(
        label=_('Tags'),
        queryset=Tag.objects.all(),
        required=False,
        widget=autocomplete.ModelSelect2Multiple(
            url=reverse_lazy('articles:tags-autocomplete'),
            attrs={'data-minimum-input-length': 2, 'data-allow-clear': True}
        )
    )


class VisualWordCloudReportForm(WordCloudReportForm):
    prefix = "visual_wc"


class CSVReportForm(_BaseAssociationForm):
    prefix = "csv"
    field_order = ['tags', 'period'] + _BaseAssociationForm.field_order + ['commented']

    commented = forms.BooleanField(label=_('Only Commented Articles'), required=False)

    period = forms.ChoiceField(label=_('Period'), required=False, choices=Period.choices, initial=Period.LAST_YEAR)

    tags = forms.ModelMultipleChoiceField(
        label=_('Tags'),
        queryset=Tag.objects.all(),
        required=False,
        widget=autocomplete.ModelSelect2Multiple(
            url=reverse_lazy('articles:tags-autocomplete'),
            attrs={'data-minimum-input-length': 2, 'data-allow-clear': True}
        )
    )


class AssociationSearchForm(forms.Form):
    q = forms.CharField(max_length=100, required=True)


class TagSearchForm(forms.Form):
    q = forms.CharField(max_length=25, required=True)


class AssociationProfileForm(forms.ModelForm):

    def clean(self):
        cleaned_data = super().clean()
        if not any(cleaned_data.values()):
            raise ValidationError(_("Form cannot be empty."), code="Empty Form")

    class Meta:
        model = AssociationProfile
        fields = ['custom_name', 'icon', 'description', 'country']


class LoginForm(AuthenticationForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_tag = False
        self.helper.disable_csrf = True


class CustomPasswordChangeForm(PasswordChangeForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_tag = False
        self.helper.disable_csrf = True
