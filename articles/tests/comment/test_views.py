from http import HTTPStatus
from typing import List

from django.contrib.auth.models import User
from django.template.response import TemplateResponse
from django.test import TestCase
from django.urls import reverse

from articles.models import Comment
from articles.tests.utils import sample_article


class CommentViewTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='jacob', email='jacob@…')
        self.wrong_list_url = reverse("articles:article-comments", kwargs={'article_id': 404})
        self.wrong_details_url = reverse("articles:article-usercomment-details", kwargs={'article_id': 404})
        self.wrong_create_url = reverse("articles:article-comment-create", kwargs={'article_id': 404})
        self.wrong_update_url = reverse("articles:article-usercomment-update", kwargs={'article_id': 404})
        self.wrong_delete_url = reverse("articles:article-usercomment-delete", kwargs={'article_id': 404})

        self.wrong_form_urls: List = [self.wrong_create_url, self.wrong_update_url, self.wrong_delete_url, ]
        self.wrong_urls = self.wrong_form_urls + [self.wrong_list_url, self.wrong_details_url]

    def test_login_required_is_most_precedence(self):
        for url in self.wrong_urls:
            responses = [self.client.get(url), self.client.put(url), self.client.post(url), self.client.delete(url)]
            for response in responses:
                self.assertEqual(response.status_code, 302)
                self.assertRedirects(response, reverse("login") + f'?next={url}')

    # TODO: test permission. Only comment author can edit or delete it. (should admin be able, too?)
    def test_wrong_article_id_returns_404(self):
        self.client.force_login(self.user)
        get_responses = [self.client.get(self.wrong_details_url), self.client.get(self.wrong_list_url)]
        for response in get_responses:
            self.assertEqual(response.status_code, HTTPStatus.NOT_FOUND)
        # post requests works with CreateView that doesn't need object pk.
        update_responses = [self.client.get(self.wrong_update_url), self.client.post(self.wrong_update_url)]
        for response in update_responses:
            self.assertEqual(response.status_code, HTTPStatus.NOT_FOUND)
        delete_response = self.client.post(self.wrong_delete_url)
        self.assertEqual(delete_response.status_code, HTTPStatus.NOT_FOUND)

    def test_create_comments_works_with_correct_templates(self):
        self.client.force_login(self.user)
        article = sample_article()
        article.save()
        url = reverse("articles:article-comment-create", kwargs={'article_id': article.id})
        self.assertTemplateUsed(self.client.get(url), 'articles/comment_form.html')
        post_response = self.client.post(url, data={"content": "Excellent Article!"})
        self.assertEqual(1, len(Comment.objects.filter(article=article, author=self.user)))
        self.assertEqual('Excellent Article!', Comment.objects.get(article=article, author=self.user).content)
        self.assertEqual(HTTPStatus.CREATED, post_response.status_code)
        self.assertTemplateUsed(post_response, 'articles/comment_detail.html')

    def test_create_response_has_correct_context(self):
        self.client.force_login(self.user)
        article = sample_article()
        article.save()
        url = reverse("articles:article-comment-create", kwargs={'article_id': article.id})
        response = self.client.post(url, data={"content": "Excellent Article!"})
        self.assertIn('article', response.context_data)
        self.assertIn('object', response.context_data)
        self.assertIn('comment', response.context_data)

    def _create_sample_article_with_comment(self):
        article = sample_article()
        article.save()
        Comment(content="Excellent!", author=self.user, article=article).save()
        self.assertEqual(1, len(Comment.objects.filter(article=article, author=self.user)))
        return article

    def test_comment_details_works(self):
        self.client.force_login(self.user)
        article = self._create_sample_article_with_comment()
        url = reverse("articles:article-usercomment-details", kwargs={'article_id': article.id})
        response = self.client.get(url)
        self.assertEqual(HTTPStatus.OK, response.status_code)
        self.assertTemplateUsed(response, 'articles/comment_detail.html')

    def test_article_is_in_commentDetails_context(self):
        self.client.force_login(self.user)
        article = self._create_sample_article_with_comment()
        url = reverse("articles:article-usercomment-details", kwargs={'article_id': article.id})
        response = self.client.get(url)
        self.assertIn('article', response.context_data)

    def test_comments_list_comes_with_correct_context(self):
        article = sample_article()
        article.save()

        self.client.force_login(self.user)
        response: TemplateResponse = self.client.get(
            reverse('articles:article-comments', kwargs={'article_id': article.id}))

        self.assertIn('comments', response.context_data)
        self.assertIn('article', response.context_data)
        self.assertIsNone(response.context_data['user_comment'])

        Comment.objects.create(author=self.user, content="John Doe wrote...", article=article)
        response: TemplateResponse = self.client.get(
            reverse('articles:article-comments', kwargs={'article_id': article.id}))
        self.assertEquals(response.context_data['user_comment'].content, "John Doe wrote...")

    def test_comments_list_renders_with_correct_template(self):
        article = sample_article()
        article.save()

        self.client.force_login(self.user)
        response: TemplateResponse = self.client.get(
            reverse('articles:article-comments', kwargs={'article_id': article.id}))

        self.assertTemplateUsed(response, template_name='articles/comment_list.html')

    def test_comment_list_excludes_user_comment(self):
        user1, user2 = self.user, User.objects.create(username="sarbaz", email="sarbaz@vatan.com")
        article = sample_article()
        article.save()
        Comment.objects.create(content="Hey! user 1", author=user1, article=article)
        Comment.objects.create(content="Hey! user 2", author=user2, article=article)

        self.client.force_login(user1)
        response: TemplateResponse = self.client.get(
            reverse('articles:article-comments', kwargs={'article_id': article.id}))

        self.assertEqual(len(response.context_data['comments']), 1)
        self.assertEqual(response.context_data['comments'][0].author, user2)

    def test_update_comments_works(self):
        self.client.force_login(self.user)
        article = self._create_sample_article_with_comment()
        url = reverse("articles:article-usercomment-update", kwargs={'article_id': article.id})
        self.assertTemplateUsed(self.client.get(url), 'articles/comment_form.html')
        post_response = self.client.post(url, data={"content": "Rubbish!"})
        self.assertEqual(1, len(Comment.objects.filter(article=article, author=self.user)))
        self.assertEqual('Rubbish!', Comment.objects.get(article=article, author=self.user).content)
        self.assertEqual(HTTPStatus.OK, post_response.status_code)
        self.assertTemplateUsed(post_response, 'articles/comment_detail.html')

    def test_update_response_has_correct_context(self):
        self.client.force_login(self.user)
        article = self._create_sample_article_with_comment()
        url = reverse("articles:article-usercomment-update", kwargs={'article_id': article.id})
        response = self.client.post(url, data={"content": "Rubbish!"})
        self.assertIn('article', response.context_data)
        self.assertIn('object', response.context_data)
        self.assertIn('comment', response.context_data)

    def test_delete_comment_works(self):
        self.client.force_login(self.user)
        article = self._create_sample_article_with_comment()
        url = reverse("articles:article-usercomment-delete", kwargs={'article_id': article.id})
        del_response = self.client.post(url)
        self.assertEqual(0, len(Comment.objects.filter(article=article, author=self.user)))
        self.assertEqual(HTTPStatus.OK, del_response.status_code)
        self.assertTemplateUsed(del_response, 'articles/comment_detail.html')

    def test_delete_response_has_correct_context(self):
        self.client.force_login(self.user)
        article = self._create_sample_article_with_comment()
        url = reverse("articles:article-usercomment-delete", kwargs={'article_id': article.id})
        response = self.client.post(url)
        self.assertIn('article', response.context_data)
        self.assertNotIn('object', response.context_data)
        self.assertNotIn('comment', response.context_data)
