from datetime import date

from django.test import TestCase

from articles.models import Article, Tag, ArticleTag
from articles.operations import auto_tag_articles


class TestAutoTagByTextSearchOperation(TestCase):
    def setUp(self) -> None:
        self.article1 = Article.objects.create(url="https://eurekalert.com/newsreleases/1", publish_date=date.today(),
                                               title="title 1", content="Some content")
        self.article2 = Article.objects.create(url="https://eurekalert.com/newsreleases/2", publish_date=date.today(),
                                               title="title 2", content="Some content")
        self.article3 = Article.objects.create(url="https://eurekalert.com/newsreleases/3", publish_date=date.today(),
                                               title="title 3", content="Some content")

    def test_no_problem_when_no_tag(self):
        auto_tag_articles([self.article1, self.article2])

    def test_meta_tags_are_not_considered(self):
        Tag.objects.create(title="meta", is_meta=True)
        self.article1.title = "meta is a new name"
        self.article1.save()
        auto_tag_articles([self.article1])

        self.assertEqual(ArticleTag.objects.count(), 0)

    def test_only_given_articles_are_tagged(self):
        Tag.objects.create(title="Covid")
        self.article1.title = "Covid is spreading again"
        self.article1.save()
        self.article2.content = "Covid vaccines"
        self.article2.save()

        auto_tag_articles([self.article1])
        self.assertIn('Covid', self.article1.tags.all().values_list('title', flat=True))
        self.assertNotIn('Covid', self.article2.tags.all().values_list('title', flat=True))

    def test_tag_title_is_regarded_as_a_phrase(self):
        Tag.objects.create(title="Covid Vaccine")

        self.article1.title = "Covid is spreading again. Vaccine needed"
        self.article1.save()
        self.article2.content = "Covid vaccines"
        self.article2.save()

        auto_tag_articles([self.article1, self.article2])
        self.assertNotIn('Covid Vaccine', self.article1.tags.all().values_list('title', flat=True))
        self.assertIn('Covid Vaccine', self.article2.tags.all().values_list('title', flat=True))

    def test_with_multiple_tags_and_articles(self):
        for tag in ['Iran', 'Health', 'Disease']:
            Tag.objects.create(title=tag)
        self.article1.title = "Iran has reduced diseases"
        self.article2.title = "Serious health problems in Oman"
        self.article2.content = "Diseases have spread in Iran"
        self.article3.title = "World cup in Qatar"
        articles = [self.article1, self.article2, self.article3]
        for article in articles:
            article.save()

        auto_tag_articles([article.id for article in articles])

        self.assertEqual(2, self.article1.tags.count())
        self.assertEqual(3, self.article2.tags.count())
        self.assertEqual(0, self.article3.tags.count())
