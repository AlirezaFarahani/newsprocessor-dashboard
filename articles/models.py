import datetime

from django.contrib.auth.models import User
from django.contrib.postgres.fields import ArrayField
from django.core.validators import MaxValueValidator, MinLengthValidator
from django.db import models
# TODO: Digest, Filter
from django.db.models.functions import Lower, Now
from django.urls import reverse
from django.utils.translation import pgettext_lazy, gettext_lazy as _
from django_countries.fields import CountryField

from articles.apps import ArticlesConfig
from articles.validators import *


# TODO: test validations using self.assertFieldOutput
class Tag(models.Model):
    # CITextField to case-insensitive fields. Only Postgres
    title = models.CharField(_("Title"), max_length=50, unique=True,
                             validators=[non_empty_english_string, MinLengthValidator(2)])
    creator = models.ForeignKey(User, null=True, blank=True,
                                verbose_name=pgettext_lazy("Tag creator", "Creator"),
                                on_delete=models.SET_NULL,
                                related_name='created_tags')
    followers = models.ManyToManyField(User, blank=True,
                                       verbose_name=pgettext_lazy("Tag followers", "Followers"),
                                       related_name="following_tags", )
    is_meta = models.BooleanField(_("Is meta"), default=False,
                                  help_text=_("Meta tags are not applied to articles automatically"))

    class Meta:
        verbose_name = _("Tag")
        verbose_name_plural = _("Tags")
        ordering = ['title']
        constraints = [
            models.UniqueConstraint(
                Lower('title'),
                name='unique case insensitive title'
            ),
        ]

    def get_absolute_url(self):
        return reverse('articles:tag-detail', kwargs={'pk': self.pk})

    def __str__(self):
        return self.title


# class AlternativeTitle(models.Model):
#     title = models.CharField(_("Title"), max_length=24, unique=True,
#                              validators=[non_empty_english_string, MinLengthValidator(2)])
#     tag = models.ForeignKey(Tag, on_delete=models.CASCADE, related_name='alternative_names')
#
#     class Meta:
#         verbose_name = _("Alternative title")
#         verbose_name_plural = _("Alternative titles")
#         ordering = ['tag__title', 'title']
#         constraints = [
#             models.UniqueConstraint(
#                 Lower('title'),
#                 name='unique case insensitive title'
#             ),
#         ]


class ArticleTag(models.Model):
    # TODO: user. settings.AUTH_USER_MODEL --> django.contrib.auth.get_user_model()
    added_by = models.ForeignKey(User, null=True, blank=True,
                                 on_delete=models.SET_NULL,
                                 related_name='applied_tags')

    tag = models.ForeignKey(Tag, on_delete=models.CASCADE)  # , related_name='used_cases')
    article = models.ForeignKey('Article', on_delete=models.CASCADE)  # , related_name='tags')

    # auto_applied = models.BooleanField(_("Auto applied"), default=False)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['tag', 'article'], name='unique_tag_per_article'),
        ]

    # noinspection PyUnresolvedReferences
    def __str__(self):
        return f"{self.tag.title} on {self.article.title[:25]}"


class JournalManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(type=Association.AssociationType.Journal)


class InstitutionManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(type=Association.AssociationType.Institution)


class FunderManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(type=Association.AssociationType.Funder)


class Association(models.Model):
    class AssociationType(models.IntegerChoices):
        Journal = 0, _('Journal')
        Institution = 1, _('Institution')
        Funder = 2, _('Funder')

    name = models.CharField(_("Name"), max_length=255, validators=[validate_non_empty_text])
    type = models.IntegerField(_("Type"), choices=AssociationType.choices)
    objects = models.Manager()  # First Manager becomes _default_manager by Django
    journals = JournalManager()
    institutions = InstitutionManager()
    funders = FunderManager()

    def type_object(self):
        return Association.AssociationType(self.type)

    def articles(self) -> models.Manager:
        if self.type == Association.AssociationType.Journal:
            return self.published_articles
        if self.type == Association.AssociationType.Institution:
            return self.researched_articles
        return self.funded_articles

    def get_absolute_url(self):
        return reverse('articles:association-profile', kwargs={'pk': self.pk})

    class Meta:
        verbose_name = _("Association")
        verbose_name_plural = _("Associations")
        constraints = [
            models.UniqueConstraint(fields=['name', 'type'], name='unique_name_per_type'),
        ]
        ordering = ['name']

    def __str__(self):
        return self.name[:50]


class AssociationProfile(models.Model):
    association = models.OneToOneField(Association, on_delete=models.CASCADE, primary_key=True, related_name='profile')
    custom_name = models.CharField(_('Persian name'), max_length=255)
    icon = models.ImageField(_('Icon'), upload_to=f'{ArticlesConfig.name}/associations/', null=True, blank=True)
    description = models.TextField(_('Description'), null=True, blank=True)
    country = CountryField(_('Country'), null=True, blank=True, blank_label=_('-- Select country --'))

    # country = models.CharField(_('Country'), choices=, null=True, blank=True)

    def __str__(self):
        return self.custom_name[:50]

    def get_absolute_url(self):
        return self.association.get_absolute_url()

    class Meta:
        verbose_name = _("Association Profile")
        verbose_name_plural = _("Association Profiles")


# TODO: test validations using self.assertFieldOutput
class Article(models.Model):
    default_pagination = 8

    url = models.URLField(_("URL"), unique=True)
    title = models.CharField(_("Title"), max_length=255, unique=True,  # avoiding duplicate or very similar articles
                             validators=[validate_non_empty_text])
    subtitle = models.TextField(pgettext_lazy("News subtitle", "Subtitle"), null=True, blank=True,
                                validators=[validate_non_empty_text])
    content = models.TextField(pgettext_lazy("News content", "Content"),
                               validators=[validate_non_empty_text])

    publish_date = models.DateField(_("Publish date"),
                                    validators=[MaxValueValidator(datetime.date.today)])
    # postgre ArrayField?
    keywords = models.JSONField(_("Keywords"), null=True, blank=True,
                                validators=[validate_json_array, MinLengthValidator(1)])
    doi = models.URLField(max_length=255, null=True, blank=True)  # some articles DO have same original articles
    original_source = models.URLField(
        _("Original Source"), null=True, blank=True, max_length=511,
        help_text='Some articles is written from other sources news site e.g. universities'
    )

    journal = models.ForeignKey(Association, null=True, blank=True,
                                verbose_name=_("Journal"),
                                limit_choices_to={'type': Association.AssociationType.Journal},
                                on_delete=models.SET_NULL, related_name='published_articles')  # TODO: related_name

    institution = models.ForeignKey(Association, null=True, blank=True,
                                    verbose_name=_("Institution"),
                                    limit_choices_to={'type': Association.AssociationType.Institution},
                                    on_delete=models.SET_NULL, related_name='researched_articles')

    funder = models.ForeignKey(Association, null=True, blank=True,
                               verbose_name=_("Funder"),
                               limit_choices_to={'type': Association.AssociationType.Funder},
                               on_delete=models.SET_NULL, related_name='funded_articles')

    tags = models.ManyToManyField(Tag, through=ArticleTag, through_fields=('article', 'tag'), related_name='articles')

    @property
    def associations(self):
        d = {}
        if self.journal:
            d['Journal'] = self.journal
        if self.funder:
            d['Funder'] = self.funder
        if self.institution:
            d['Institution'] = self.institution
        return d

    class Meta:
        verbose_name = _("Article")
        verbose_name_plural = _("Articles")
        ordering = ["-publish_date"]
        constraints = [
            models.CheckConstraint(check=models.Q(publish_date__lte=Now(output_field=models.DateField())),
                                   name="article_publish_date")
        ]
        indexes = [
            models.Index(fields=['-publish_date']),
        ]

    def __str__(self):
        title = str(self.title)
        return title if (len(title)) < 100 else title[:97] + "..."

    # class Meta:
    #     constraints = [
    #         # https://docs.djangoproject.com/en/3.2/ref/models/options/#constraints
    #         models.CheckConstraint(check=models.Q(age__gte=18), name='age_gte_18'),
    #     ]

    # we can define funder and others as Proxy or base Association as Meta/Abstract class.

    # @property
    # def funder(self):
    # def journal(self):
    # def institution(self):

    # def save(self, force_insert=False, force_update=False, using=None,
    #          update_fields=None):
    #     validate_json_array(self.keywords.value_from_object(self))
    #     validate_non_empty(self.keywords.value_from_object(self))
    #     super(Article, self).save(force_insert, force_update, using, update_fields)


class Comment(models.Model):
    content = models.TextField(_("Content"), max_length=500, validators=[validate_non_empty_text])
    author = models.ForeignKey(User, verbose_name=_("Author"),
                               on_delete=models.CASCADE,
                               related_name='comments')
    article = models.ForeignKey(Article, verbose_name=_("Article"),
                                on_delete=models.CASCADE,
                                related_name='comments')

    class Meta:
        verbose_name = _("Comment")
        verbose_name_plural = _("Comments")
        ordering = ["-author__username"]  # TODO: -update_time
        constraints = [
            models.UniqueConstraint(fields=['author', 'article'], name='unique_user_comment_per_article'),
        ]
        # indexes = [
        #     models.Index(fields=['-create_time']),
        # ]

    def __str__(self):
        return self.content[:100]
        # return _("%(author)s wrote %(comment)") % {"comment": self.content[:100], "author": self.author.username}


class Digest(models.Model):
    # class DigestType(models.IntegerChoices):
    #     Daily = 0, _('Daily')
    #     Weekly = 1, _('Weekly')
    #     Monthly = 2, _('Monthly')

    recipient = models.ForeignKey(User, verbose_name=_("Recipient"), related_name="digests", on_delete=models.CASCADE)
    articles = models.ManyToManyField(Article, verbose_name=_("Digest's articles"))
    # type = models.IntegerField(choices=DigestType.choices)
    creation_date = models.DateField(_("Creation Date"))

    class Meta:
        verbose_name = _("Digest")
        verbose_name_plural = _("Digests")
        ordering = ["-creation_date"]
        constraints = [
            models.CheckConstraint(
                check=models.Q(creation_date__lte=Now(output_field=models.DateField())),
                name="correct_digest_date"),
        ]

    def __str__(self):
        return f"user {self.recipient.username}'digest on {self.creation_date.strftime('%Y/%m/%d')}"
