import base64
from abc import ABC, abstractmethod
from io import BytesIO
from textwrap import dedent
from typing import List, Type, Dict

import numpy as np
import plotly.graph_objects as go
from _plotly_utils.colors import sample_colorscale
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Div
from django.contrib.auth.mixins import LoginRequiredMixin
from django.forms import Form, Media
from django.templatetags.static import static
from django.urls import reverse
from django.utils.translation import gettext as _
from django.views import View
from django.views.generic import TemplateView
from matplotlib import pyplot as plt
from plotly.subplots import make_subplots

from articles.forms import (MostUsedTagsReportForm, TagTrendReportForm, TagCoOccurrenceReportForm,
                            WordCloudReportForm, CSVReportForm, VisualWordCloudReportForm, Period)
from articles.mixins import (ArticleWordCloud, TextualWordCloudExport, CSVExport, DefaultNewsCSVWriter)
from articles.operations import (tag_trend, most_used_tags, tag_co_occurrence, word_cloud_query, csv_query)


class ReportsTabView(LoginRequiredMixin, TemplateView):
    template_name = 'articles/tab_reports.html'

    def get_context_data(self, **kwargs):
        context = {
            'forms': [
                # if not setting auto_id, DAL widget see multiple inputs with same id during initialization
                {'form': TagTrendReportForm(), 'form_helper': ReportFormHelper,
                 'action': reverse('articles:reports-tag-trend'), 'title': _('Tag Usage Trend'),
                 'name': 'tag_trend'},
                {'form': MostUsedTagsReportForm(), 'form_helper': ReportFormHelper,
                 'action': reverse('articles:reports-most-used-tags'), 'title': _('Most Used Tags'),
                 'name': 'most_used'},
                {'form': TagCoOccurrenceReportForm(), 'form_helper': ReportFormHelper,
                 'action': reverse('articles:reports-tag-co-occurrence'), 'title': _('Tag Co-Occurrence'),
                 'name': 'co_oc'},
                {'form': VisualWordCloudReportForm(), 'form_helper': ReportFormHelper,
                 'action': reverse('articles:reports-visual-wordcloud'), 'title': _('Visual Word cloud'),
                 'name': 'visual_wc'},
                {'form': WordCloudReportForm(), 'form_helper': ReportFormHelper,
                 'action': reverse('articles:reports-textual-wordcloud'), 'title': _('Textual Word cloud'),
                 'name': 'textual_wc'},
                {'form': CSVReportForm(), 'form_helper': CsvReportFormHelper,
                 'action': reverse('articles:reports-csv'), 'title': 'CSV',
                 'name': 'csv'},
            ],
        }
        for obj in context['forms']:
            obj['form_helper'] = obj['form_helper'](obj['form'])
        media = Media()
        for form_item in context['forms']:
            media = media + form_item['form'].media
        context['forms_media'] = media
        context.update(kwargs)
        return super().get_context_data(**context)


class AbcPlotlyPlotReportView(LoginRequiredMixin, TemplateView, ABC):
    # noinspection PyAttributeOutsideInit
    def get(self, request, *args, **kwargs):
        self.form = self.form_class(self.request.GET)
        self.form.full_clean()
        return super().get(request, *args, **kwargs)

    @property
    @abstractmethod
    def form_class(self) -> Type[Form]:
        pass

    @abstractmethod
    def draw_plot(self) -> go.Figure:
        pass

    # noinspection PyMethodMayBeStatic
    def mode_bars_to_remove(self) -> List[str]:
        return ['select', 'lasso2d', 'zoom']

    def get_context_data(self, **kwargs):
        fig = self.draw_plot()
        fig.update_layout(modebar_remove=self.mode_bars_to_remove())
        plot_name = self.get_plot_name(**self.form.cleaned_data)
        config = {
            "displaylogo": False,
            'toImageButtonOptions': {
                'format': 'jpeg',  # one of png, svg, jpeg, webp
                'filename': plot_name,
            }
        }
        fig_html = fig.to_html(full_html=False, config=config,
                               include_plotlyjs=static('js/plotly-2.8.3.min.js'))  # include_plotlyjs='CDN'))
        return super().get_context_data(plot=fig_html, plot_name=plot_name, **kwargs)

    @abstractmethod
    def get_plot_name(self, **kwargs):
        pass


colorscale = sample_colorscale('Blues', [0.4, 1])


def _filters_i18n(d: Dict[str, object]) -> List[str]:
    return [f"{_(k.title())}: {v}" for k, v in d.items() if v]


class TagTrendReportView(AbcPlotlyPlotReportView):
    template_name = 'articles/reports_plotly_plot.html'

    @property
    def form_class(self) -> Type[Form]:
        return TagTrendReportForm

    def get_plot_name(self, tag, period: str, **kwargs):
        return _("Usages of tag '%(tag)s' in %(period)s") % {
            'tag': tag.title, 'period': Period(period).label
        } + "".join([f'\n{item}' for item in _filters_i18n(kwargs)])

    def draw_plot(self) -> go.Figure:
        df, sunburst_df = tag_trend(**self.form.cleaned_data)
        sunburst_graph = sunburst_df is not None
        fig = make_subplots(
            rows=2 if sunburst_graph else 1, cols=1,
            vertical_spacing=0.1,
            subplot_titles=[_("Tag usage trend"), _("Tag usages by associations")],
            row_heights=[0.4, 0.6] if sunburst_graph else [1],
            specs=[
                [{"type": "bar"}],
                [{"type": "sunburst"}],
            ] if sunburst_graph else [
                [{"type": "bar"}]
            ]
        )
        # fig = go.Figure([go.Scatter(
        fig.add_trace(
            go.Scatter(
                x=df['period'],
                y=df['usages'] / df['articles'],
                hovertemplate=dedent("""\
            <b>Time</b>: %{x}<br>
            Usages: %{customdata[0]}<br>
            Articles: %{customdata[1]}<br>
            Usage percent: %{y:.1%}
            <extra></extra>
            """),  # <extra></extra> to hide trace0 in second box of hover
                customdata=np.stack((df['usages'], df['articles']), axis=-1),
                # https://stackoverflow.com/q/69278251/1660013
                mode='lines+markers',
                marker={'size': 10}
            ),
            row=1, col=1
        )
        fig.update_yaxes(title_text=_("Tag usage percent over queried articles"), row=1, col=1)
        if sunburst_graph:
            fig = fig.add_trace(
                go.Sunburst(
                    text=sunburst_df['name'].map(lambda s: s[:15] + "..." if (len(s) >= 15) else s),
                    labels=sunburst_df['name'],
                    values=sunburst_df['usages'],
                    parents=sunburst_df['type'],
                    branchvalues="total",
                    textinfo="text",
                    hovertemplate="%{label}<br>Usages:%{value} (%{percentEntry:.1%})<extra></extra>",
                    # hoverinfo="label+value+percent",
                ),
                row=2, col=1,
            )
        fig.update_layout(
            xaxis_title=None,
            height=1000 if sunburst_graph else 450,
            margin=dict(t=24, r=12, l=12)
        )
        fig.update_yaxes(autorange=True,
                         tickangle=45,
                         tickformat='p',
                         title_standoff=28, )
        fig.update_xaxes(tickangle=45, title_standoff=28)
        fig.update_yaxes(automargin=True)
        fig.update_xaxes(automargin=True)
        return fig


class MostUsedTagsReportView(AbcPlotlyPlotReportView):
    template_name = 'articles/reports_plotly_plot.html'

    @property
    def form_class(self) -> Type[Form]:
        return MostUsedTagsReportForm

    def get_plot_name(self, period, **kwargs):
        return _("Most used tags in %(period)s") % {
            'period': Period(period).label
        } + "".join([f'\n{item}' for item in _filters_i18n(kwargs)])

    def draw_plot(self) -> go.Figure:
        df, articles_in_period = most_used_tags(**self.form.cleaned_data)
        fig = go.Figure([go.Bar(
            x=df['title'], y=df['usages'],
            marker={
                'color': df['usages'] / articles_in_period,
                'cmin': 0,
                'cmax': df['usages'][0] / articles_in_period,
                'colorscale': colorscale,
                'colorbar': {
                    'nticks': 5,
                    'tickformat': 'p'
                }
            })
        ])
        fig.update_layout(
            xaxis_title=None,
            yaxis_title=_("Number of tag appliances on articles"),
        )
        fig.update_yaxes(title_standoff=28)
        # fig.update_yaxes(automargin=True)  # extra space between ticks and y-axis title
        return fig


class TagCoOccurrenceReportView(AbcPlotlyPlotReportView):
    template_name = 'articles/reports_plotly_plot.html'

    @property
    def form_class(self) -> Type[Form]:
        return TagCoOccurrenceReportForm

    def get_plot_name(self, tag, period: str, **kwargs):  # TODO: period
        return _("Tags most used with '%(tag)s' in %(period)s") % {
            'tag': tag.title, 'period': Period(period).label
        } + "".join([f'\n{item}' for item in _filters_i18n(kwargs)])

    def draw_plot(self) -> go.Figure:
        df, tag_usages = tag_co_occurrence(**self.form.cleaned_data)  # usages in filtered query

        fig = go.Figure([
            go.Bar(x=df['title'], y=df['cooc'], marker={
                'color': df['cooc'] / tag_usages,
                'cmin': 0,
                'cmax': df['cooc'][0] / tag_usages,
                'colorscale': colorscale,
                'colorbar': {
                    'nticks': 5,
                    'tickformat': 'p'
                }
            })
        ])
        fig.update_layout(
            xaxis_title=None,
            yaxis_title=_("Number of co-occurrences"),
        )
        fig.update_yaxes(automargin=True)  # extra space between ticks and y-axis title
        fig.update_xaxes(automargin=True)  # extra space between ticks and y-axis title
        return fig


class VisualWordCloudReportView(LoginRequiredMixin, TemplateView):
    template_name = 'articles/reports_image.html'

    def get(self, request, *args, **kwargs):
        self.form = VisualWordCloudReportForm(self.request.GET)
        self.form.full_clean()
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        wc = ArticleWordCloud(word_cloud_query(**self.form.cleaned_data))
        fig: plt.Figure = wc.get_visual_repr()
        buffer = BytesIO()
        fig.savefig(buffer, format='jpg')
        buffer.seek(0)
        image_jpg = buffer.getvalue()
        buffer.close()
        graphic = base64.b64encode(image_jpg)
        graphic = graphic.decode('utf-8')
        return super().get_context_data(embedded_img=graphic, **kwargs)


class TextualWordCloudReportView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        self.form = WordCloudReportForm(self.request.GET)
        self.form.full_clean()
        wc = ArticleWordCloud(word_cloud_query(**self.form.cleaned_data))
        return TextualWordCloudExport(wc).serve_export(request)


class CSVReportView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        self.form = CSVReportForm(self.request.GET)
        self.form.full_clean()
        csv_writer = DefaultNewsCSVWriter(csv_query(**self.form.cleaned_data))
        return CSVExport(csv_writer).serve_export(request)


class ReportFormHelper(FormHelper):
    form_tag = False
    disable_csrf = True
    label_class = 'w-100'
    field_class = 'w-75'
    include_media = False  # since multiple 'report form' are used in single page.
    # label_class = 'col-3'
    # field_class = 'col-7'  # flex-grow-1, must be used with form-horizontal to have effect
    # form_class = 'form-horizontal'  # non-functional in BS5, but crispy-BS5 still needs it


class CsvReportFormHelper(ReportFormHelper):
    def __init__(self, form=None):
        super().__init__(form)
        # Forcing checkbox to be a full row to have a better UI. Checkbox label is floated (display inline) by default
        self['commented'].wrap_once(Div, css_class='col-12')
