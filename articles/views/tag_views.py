from http import HTTPStatus

from dal import autocomplete
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.db import IntegrityError
from django.db.models import Case, When, Value, Count
from django.db.models.functions import Coalesce, Length
from django.http import HttpResponse, HttpResponseBadRequest, HttpRequest, JsonResponse
from django.shortcuts import get_object_or_404
from django.utils.translation import gettext as _
from django.views import View
from django.views.decorators.http import require_POST
from django.views.generic import ListView, CreateView
from django.views.generic.list import BaseListView

from articles.forms import TagCreationForm, TagModelForm, TagSearchForm
from articles.models import Tag, Article, ArticleTag
from articles.operations import create_and_auto_apply_tag
# If your want to filter tags and show the same template:
# https://docs.djangoproject.com/en/3.2/topics/class-based-views/generic-display/#dynamic-filtering
from articles.views import SortableListMixin


class BaseTagListView(LoginRequiredMixin, SortableListMixin, ListView):
    model = Tag
    paginate_by = 30
    ordering = "title"
    queryset = Tag.objects.annotate(usages=Coalesce(Count('articletag'), 0))
    sort_keys = [("usages", _("Usage count")), ("title", _("Title"))]

    # noinspection PyAttributeOutsideInit
    def get(self, request, *args, **kwargs):
        if request.GET and 'q' in request.GET:
            self.form = TagSearchForm(request.GET)
        else:
            self.form = TagSearchForm()
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        qs = super().get_queryset()
        if self.form.is_valid():
            return qs.filter(title__icontains=self.form.cleaned_data['q'])
        return qs

    def get_context_data(self, *, object_list=None, **kwargs):
        return super().get_context_data(form=self.form, object_list=object_list, **kwargs)

    # fixme: to have a cleaner code, we can extend ModelManager for Tag and put it there as a custom method.
    def get_follow_status_expression(self):
        user: User = self.request.user
        user_followed_tags = list(user.following_tags.all().values_list('id', flat=True))
        return Case(
            When(id__in=user_followed_tags, then=Value(True)),
            default=Value(False)
        )


class AllTagsListView(BaseTagListView):
    extra_context = {'active_menu': 'all_tags'}
    template_name = 'articles/tab_tags.html'

    def get_queryset(self):
        return super().get_queryset().annotate(
            is_followed=self.get_follow_status_expression()
        )


class CreatedByMeTagsListView(BaseTagListView):
    extra_context = {'active_menu': 'my_tags'}
    template_name = 'articles/tab_tags.html'

    def get_queryset(self):
        return super().get_queryset().filter(creator=self.request.user).annotate(
            is_followed=self.get_follow_status_expression()
        )


class FollowedByMeTagsListView(BaseTagListView):
    extra_context = {'active_menu': 'my_followed_tags'}
    template_name = 'articles/tab_tags.html'

    def get_queryset(self):
        return super().get_queryset().filter(followers=self.request.user).annotate(
            is_followed=Value(True)
        )


class MetaTagsListView(BaseTagListView):
    extra_context = {'active_menu': 'meta_tags'}
    template_name = 'articles/tab_tags.html'

    def get_queryset(self):
        return super().get_queryset().filter(is_meta=True).annotate(
            is_followed=self.get_follow_status_expression()
        )


class ArticleTagsListView(LoginRequiredMixin, ListView):
    model = Tag
    template_name = 'articles/article_tags.html'

    def get_queryset(self):
        article = get_object_or_404(Article, pk=self.kwargs['article_id'])
        return article.tags.all()

    def get_context_data(self, *, object_list=None, **kwargs):
        if 'article' not in kwargs:
            kwargs['article'] = get_object_or_404(Article, pk=self.kwargs.get('article_id'))
        return super().get_context_data(**kwargs)


class TagCreateView(LoginRequiredMixin, CreateView):
    template_name = 'articles/tags_create.html'
    form_class = TagCreationForm
    model = Tag

    def form_valid(self, form: TagCreationForm):
        form.instance.creator = self.request.user
        try:
            create_and_auto_apply_tag(form.save())
            messages.success(self.request, _("Tag created successfully"))
        except ValueError as e:
            return self.form_invalid(form)
        # bypass redirection mechanism and return empty form
        # sacrificing 'initials and prefix' logic in FormMixin#get_form_kwargs

        return self.render_to_response(self.get_context_data(form=TagCreationForm()), status=201)


# endregion

# region Follow Tag

# noinspection PyMethodMayBeStatic
# message = _("You are already following this tag.")
# messages.error(request, message)
# return TemplateResponse(request, template='notifications.html', status=HTTPStatus.BAD_REQUEST)
class ToggleTagFollowershipView(LoginRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        tag = get_object_or_404(Tag, pk=kwargs['pk'])
        if tag.followers.filter(pk=request.user.id).exists():
            return HttpResponseBadRequest(_("You are already following this tag."))
        tag.followers.add(request.user)
        return HttpResponse(status=HTTPStatus.CREATED)

    def delete(self, request, *args, **kwargs):
        tag = get_object_or_404(Tag, pk=kwargs['pk'])
        if not tag.followers.filter(pk=request.user.id).exists():
            return HttpResponseBadRequest(_("You are not following this tag."))
        tag.followers.remove(request.user)
        return HttpResponse()


class TagAutocompleteView(LoginRequiredMixin, autocomplete.Select2QuerySetView):
    paginate_by = 10

    def get_queryset(self):
        qs = Tag.objects.all()

        if self.q:
            qs = qs.filter(title__icontains=self.q).order_by(Length('title'), 'title')

        return qs[:50]


# ------------------------------------------ ArticleTag views ----------------------------------------------------------


# region ArticleTags
# TODO: json response? https://docs.djangoproject.com/en/3.2/topics/class-based-views/mixins/#more-than-just-html
# TODO: permissions. Only tag owner and admins

# https://docs.djangoproject.com/en/3.2/topics/auth/default/#the-permissionrequiredmixin-mixin
@login_required
@require_POST
def create_tag_and_add_to_article(request: HttpRequest, article_id: int):
    article = get_object_or_404(Article, pk=article_id)
    form = TagModelForm(data=request.POST)
    try:
        form.instance.creator = request.user
        tag = create_and_auto_apply_tag(form.save())
        ArticleTag.objects.create(article=article, tag=tag, added_by=request.user)
        return HttpResponse(status=HTTPStatus.CREATED)
    except IntegrityError as ie:
        # tag is already applied through #auto_apply_tag_after_creation
        return HttpResponse(status=HTTPStatus.CREATED)
    except ValueError:
        # return TemplateResponse(request, 'articles/article_tag_create_form.html', context={'form': form})
        return JsonResponse(form.errors.get_json_data(), status=HTTPStatus.BAD_REQUEST)


# noinspection PyMethodMayBeStatic
class AddOrRemoveArticleTagView(LoginRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        article = get_object_or_404(Article, pk=kwargs['article_id'])
        tag = get_object_or_404(Tag, pk=kwargs['tag_id'])
        ArticleTag.objects.create(article=article, tag=tag, added_by=request.user)
        return HttpResponse(status=HTTPStatus.OK)

    def delete(self, request, *args, **kwargs):
        ArticleTag.objects.filter(article_id=kwargs['article_id'], tag_id=kwargs['tag_id']).delete()
        return HttpResponse(status=HTTPStatus.OK)


# Used by 'tom-select' component in adding tags to article in article_details page
class ArticleTagSearchView(LoginRequiredMixin, BaseListView):
    limit = 5

    def get_queryset(self):
        article = get_object_or_404(Article, pk=self.kwargs['article_id'])
        search_term = self.request.GET['q']
        query = Tag.objects.filter(title__icontains=search_term).exclude(articles=article)
        return query[:self.limit]

    def get(self, request, *args, **kwargs):
        tags = {'items': [tag for tag in self.get_queryset().values('id', 'title')]}
        return JsonResponse(tags)

# endregion
