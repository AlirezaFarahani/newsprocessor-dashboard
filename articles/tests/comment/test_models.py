from django.contrib.auth.models import User
from django.db import IntegrityError
from django.test import TestCase

from articles.models import Comment
from articles.tests.utils import sample_article, sample_article2


class TestComment(TestCase):

    def setUp(self):
        self.user = User.objects.create_user(username="jacob", email="a@b.com")
        self.article = sample_article()
        self.article.save()

    def test_unique_user_comment_per_article(self):
        Comment.objects.create(article=self.article, author=self.user, content="Excellent article!")
        with self.assertRaises(IntegrityError):
            Comment.objects.create(article=self.article, author=self.user, content="WOW!")

    def test_comments_are_deleted_after_user_deletion(self):
        article2 = sample_article2()
        article2.save()
        Comment.objects.create(article=self.article, author=self.user, content="Excellent article!")
        Comment.objects.create(article=article2, author=self.user, content="Rubbish!")
        user_id = self.user.id
        self.assertEqual(2, len(Comment.objects.filter(author_id=user_id)))

        self.user.delete()
        self.assertEqual(0, len(Comment.objects.filter(author_id=user_id)))

    def test_comments_are_deleted_after_article_deletion(self):
        user2 = User.objects.create_user(username="alialavi", email="a@b.com")
        Comment.objects.create(article=self.article, author=self.user, content="Excellent article!")
        Comment.objects.create(article=self.article, author=user2, content="Rubbish!")
        article_id = self.article.id
        self.assertEqual(2, len(Comment.objects.filter(article_id=article_id)))

        self.article.delete()
        self.assertEqual(0, len(Comment.objects.filter(article_id=article_id)))
