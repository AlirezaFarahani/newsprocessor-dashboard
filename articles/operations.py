import logging
from datetime import date
from datetime import timedelta
from itertools import chain
from typing import Optional, Tuple, List, Union

import pandas as pd
from django.contrib.postgres.aggregates import StringAgg
from django.contrib.postgres.search import SearchQuery, SearchVectorField, SearchRank, SearchVector
from django.db import IntegrityError, transaction
from django.db.models import QuerySet, Value, Count, Exists, OuterRef, Q, TextField, FloatField
from django.db.models.expressions import RawSQL, F, When, Case
from django.db.models.functions import TruncMonth, TruncYear, TruncDay, Substr
from django.utils.dateparse import parse_datetime
from pandas.tseries import offsets

from articles.forms import Period
from articles.models import Article, Tag, ArticleTag, Comment, Association

digest_logger = logging.getLogger('digest')
tagging_logger = logging.getLogger('tagging')


def save_crawled_articles(article_json: dict):
    if 'url' not in article_json:
        raise ValueError('malformed article without url skipped')

    required_keys = ['title', 'content', 'date']
    for key in required_keys:
        if key not in article_json:
            raise ValueError('Malformed article %s not having %s field', article_json["url"], key)

    try:
        date_string = article_json['date']['$date']
    except TypeError:
        raise ValueError(f"Error in parsing date field: {article_json['date']}")
    # python doesn't read timezone info '2021-07-02T00:00:00.000Z' and we don't need hour/minute/second info
    # date_string = date_string[:date_string.find("T")]

    query_set: QuerySet = Article.objects.filter(url=article_json['url'])
    if not query_set:
        article = Article(url=article_json['url'])
    else:
        article = query_set.get()
    article.title = article_json['title']
    article.content = article_json['content']
    article.publish_date = parse_datetime(date_string).date()

    # optional fields
    if 'subtitle' in article_json:
        article.subtitle = article_json['subtitle']
    if 'keywords' in article_json:
        article.keywords = article_json['keywords']
    if 'journal_article_url' in article_json:
        article.doi = article_json['journal_article_url']
    if 'original_source_url' in article_json:
        article.original_source = article_json['source_article_url']

    # associations
    if 'journal' in article_json:
        journal, created = Association.objects.get_or_create(
            name=article_json['journal'], type=Association.AssociationType.Journal
        )
        article.journal = journal

    if 'funder' in article_json:
        funder, created = Association.objects.get_or_create(
            name=article_json['funder'], type=Association.AssociationType.Funder
        )
        article.funder = funder

    if 'institution' in article_json:
        institution, created = Association.objects.get_or_create(
            name=article_json['institution'], type=Association.AssociationType.Institution
        )
        article.institution = institution

    article.full_clean()  # raise exception if integrity or validation error exist, before saving in db.
    article.save()
    return article


def auto_tag_articles(articles_ids: List[Union[int, Article]]):
    articles_ids = [item.id if isinstance(item, Article) else item for item in articles_ids]
    imported_articles: QuerySet[Article] = Article.objects.filter(id__in=articles_ids)
    with transaction.atomic():
        article_tags = []
        for tag in Tag.objects.exclude(is_meta=True):  # tag: Tag
            matched_articles = simple_article_search_queryset(f'"{tag.title}"', filtered_query_set=imported_articles)
            for article in matched_articles:
                article_tags.append(ArticleTag(tag=tag, article=article))
        ArticleTag.objects.bulk_create(article_tags, ignore_conflicts=True)


# Two things happen when a tag gets created.
# 1. The user creating the tag follows it by default.
# 2. All existing articles matching that tag (having tag's title in their article title or content) must be tagged with
# that tag automatically.
def create_and_auto_apply_tag(tag: Tag) -> Tag:
    if not tag.pk:  # if tag is not saved in db, do it. (Not 100% correct solution)
        tag.save()
    try:
        tag.followers.add(tag.creator)
    except IntegrityError as e:
        if not tag.creator:
            tagging_logger.warning("Tag creator shouldn't be empty unless created from scripts.")
        else:
            raise e
    apply_tag_on_matching_news(tag)
    return tag


# TODO: create tag operation.
# create and follow at the same time. Refactor the code to use this operation
def apply_tag_on_matching_news(tag: Tag):
    if tag.is_meta:
        return
    """ A tag must be applied automatically on all articles matching the tag's title after it is created """
    matched_articles = simple_article_search_queryset(f'"{tag.title}"')  # treat tag title as a phrase query
    with transaction.atomic():
        batch_size = 1000
        page_number = 0
        while (articles := matched_articles[page_number * batch_size: (page_number + 1) * batch_size]).exists():
            applying_tags = (ArticleTag(tag=tag, article=article) for article in articles)
            page_number += 1
            ArticleTag.objects.bulk_create(applying_tags, ignore_conflicts=True)


# def create_digest(user: User):
#     # we can read digest-creation interval from user setting.
#     last_digest: Digest = user.digests.order_by('creation_date').first()
#     if last_digest and last_digest.creation_date >= date.today():
#         digest_logger.warning(f'Ignoring digest in less than a day for user {user.username} (id={user.id})')
#         return
#
#     related_articles: QuerySet = Article.objects.filter(publish_date__gte=yesterday(), tags__followers=user).distinct()
#     already_digested: QuerySet = Article.objects.filter(digest__recipient=user)
#     related_articles = related_articles.difference(already_digested).order_by("-publish_date")[:20]
#     if not related_articles.exists():
#         digest_logger.info(f"No articles matched for {user.username}(id={user.id})'s digest.")
#         return
#     digest = Digest.objects.create(recipient=user, creation_date=date.today())
#     digest.articles.add(*related_articles)
#
#
# def create_user_digests():
#     for user in User.objects.all():
#         create_digest(user)


def simple_article_search_queryset(q: str, filtered_query_set=Article.objects.all()) -> QuerySet[Article]:
    ts_query = SearchQuery(q, search_type='websearch', config=Value('english'))
    return filtered_query_set \
        .annotate(ts=RawSQL('vector_column', params=[], output_field=SearchVectorField())) \
        .annotate(rank=SearchRank(vector=F('ts'),
                                  query=ts_query,
                                  normalization=Value(32))) \
        .filter(ts=ts_query)


def advanced_article_search(include_comments=False, q: Optional[str] = None,
                            journal: Optional[Association] = None, institution: Optional[Association] = None,
                            funder: Optional[Association] = None):
    def _advanced_search_with_comments():
        qs = Article.objects.all()
        if any(article_filters.values()):
            qs = qs.filter(**{k: v for k, v in article_filters.items() if v})
        if q and q.strip():
            comments_qs = Article.objects.annotate(
                all_comments=SearchVector(StringAgg('comments__content', delimiter='', output_field=TextField()),
                                          weight='A', config='english')
            ).filter(all_comments=SearchQuery(q, config='english', search_type='websearch'))
            ts_query = SearchQuery(q, search_type='websearch', config=Value('english'))
            qs = qs.annotate(ts=RawSQL('vector_column', params=[], output_field=SearchVectorField()))
            article_rank = SearchRank(vector=F('ts'), query=ts_query, normalization=Value(32))
            comment_rank = Value(1, output_field=FloatField())
            qs = qs.filter(ts=ts_query)
            qs = qs | Article.objects.filter(id__in=comments_qs)
            qs = qs.annotate(rank=Case(When(id__in=comments_qs, then=comment_rank), default=article_rank))
            qs = qs.order_by('-rank')

        return qs

    def _default_advanced_search():
        qs = Article.objects.all()
        if any(article_filters.values()):
            qs = qs.filter(**{k: v for k, v in article_filters.items() if v})
        if q and q.strip():
            qs = simple_article_search_queryset(q, qs).order_by('-rank')
        return qs

    article_filters = {'journal': journal, 'institution': institution, 'funder': funder}
    if include_comments:
        return _advanced_search_with_comments()
    else:
        return _default_advanced_search()


def _period_to_date_filter(period: Period) -> Optional[date]:
    today = date.today()
    if period == Period.LAST_WEEK:
        return today - timedelta(7)
    elif period == Period.LAST_MONTH:
        return today - timedelta(30)
    elif period == Period.LAST_YEAR:
        return today.replace(year=today.year - 1)
    elif period == Period.ALL_TIME:
        return None
    else:
        raise ValueError()


def _filter_queryset_by_period(qs: QuerySet, period: str, *, lookup_prefix=''):
    if period:
        period_filter = _period_to_date_filter(Period(period))
        if period_filter:
            filter_prefix = f"{lookup_prefix}__" if lookup_prefix else ''
            qs = qs.filter(**{f'{filter_prefix}publish_date__gte': period_filter})
    return qs


def _filter_queryset_by_assocs(qs: QuerySet, *, lookup_prefix='', **assoc_filters):
    valid_assocs = ('journal', 'institution', 'funder')
    filter_prefix = f"{lookup_prefix}__" if lookup_prefix else ''
    applied_assocs = {f'{filter_prefix}{item}': assoc for item in valid_assocs if (assoc := assoc_filters.get(item))}
    if applied_assocs:
        qs = qs.filter(**applied_assocs)
    return qs


def _filter_article_queryset_by_tags(qs: QuerySet[Article], tags):
    if tags:
        for tag in tags:  # 'relational division'. articles relating to all these tags
            qs = qs.filter(Exists(ArticleTag.objects.filter(article_id=OuterRef('id')).filter(tag_id=tag.id)))
    return qs


def tag_trend(tag, period, **kwargs) -> Tuple[pd.DataFrame, Optional[pd.DataFrame]]:
    today = date.today()

    periods = [Period.ALL_TIME, Period.LAST_YEAR, Period.LAST_MONTH, Period.LAST_WEEK]
    truncs = [TruncYear, TruncMonth, TruncDay, TruncDay]
    date_formats = ['%Y', '%b-%Y', '%b-%d', '%b-%d']
    date_ranges = [
        pd.date_range(start=Article.objects.order_by('publish_date')[0].publish_date - offsets.YearBegin(),
                      end=today, freq='YS'),
        pd.date_range(end=today, periods=12, freq='MS'),
        pd.date_range(end=today - timedelta(days=1), periods=30, freq='D'),
        pd.date_range(end=today - timedelta(days=1), periods=7, freq='D')
    ]

    idx = periods.index(period)
    start_date = date_ranges[idx][0].date()
    trunc = truncs[idx]
    date_bins = date_ranges[idx]
    date_format = date_formats[idx]

    articles_qs = Article.objects.all()
    tag_qs = tag.articles.all()
    if any(kwargs.values()):
        tag_qs = tag_qs.filter(**{k: v for k, v in kwargs.items() if v})
        articles_qs = articles_qs.filter(**{k: v for k, v in kwargs.items() if v})
    if start_date:
        tag_qs = tag_qs.filter(publish_date__gte=start_date)
        articles_qs = articles_qs.filter(publish_date__gte=start_date)

    articles_qs = articles_qs.annotate(period=trunc('publish_date')).values('period') \
        .annotate(usages=Count('*'))
    tag_qs = tag_qs.annotate(period=trunc('publish_date')).values('period') \
        .annotate(usages=Count('*'))

    # It could be possible that aggregation query returns nothing for some periods.
    # We create all possible periods with a default value of zero and then update it with aggregation query results.
    df = pd.DataFrame(data=[[0, 0] for _ in date_bins],
                      index=[date_bin.date() for date_bin in date_bins],
                      columns=['usages', 'articles'])
    for record in tag_qs:
        df.at[record['period'], 'usages'] = record['usages']
    for record in articles_qs:
        df.at[record['period'], 'articles'] = record['usages']
    df['period'] = df.index.map(lambda x: x.strftime(date_format))

    if not any(kwargs.values()):  # no assoc filter, draw pie chart
        # journal_qs = Association.journals.annotate(
        #     usages=Count('id', filter=Q(published_articles__articletag__tag=tag))
        # ).values('name', 'type', 'usages')
        journal_qs = Association.journals.filter(published_articles__publish_date__gte=start_date).annotate(
            usages=Count('id', filter=Q(published_articles__articletag__tag=tag))
        ).order_by('-usages').values('name', 'type', 'usages')[:10]
        institution_qs = Association.institutions.filter(researched_articles__publish_date__gte=start_date).annotate(
            usages=Count('id', filter=Q(researched_articles__articletag__tag=tag))
        ).order_by('-usages').values('name', 'type', 'usages')[:10]
        funder_qs = Association.funders.filter(funded_articles__publish_date__gte=start_date).annotate(
            usages=Count('id', filter=Q(funded_articles__articletag__tag=tag))
        ).order_by('-usages').values('name', 'type', 'usages')[:10]
        # pie_qs = QuerySet.union(journal_qs, institution_qs, funder_qs)
        pie_df = pd.DataFrame.from_records(chain(
            journal_qs, institution_qs, funder_qs,
            [{'name': 'Journal', 'type': "",
              "usages": Association.journals.filter(
                  published_articles__publish_date__gte=start_date, published_articles__tags=tag
              ).count()},
             {'name': 'Institution', 'type': "",
              "usages": Association.institutions.filter(
                  researched_articles__publish_date__gte=start_date, researched_articles__tags=tag
              ).count()},
             {'name': 'Funder', 'type': "", "usages": Association.funders.filter(
                  funded_articles__publish_date__gte=start_date, funded_articles__tags=tag
             ).count()}]
        ))
        pie_df['type'] = pie_df['type'].map(lambda x: Association.AssociationType(x).name if x != "" else x)
        return df, pie_df
    return df, None


def most_used_tags(period: Period, **article_filters) -> Tuple[pd.DataFrame, int]:
    qs = ArticleTag.objects.all()
    qs = _filter_queryset_by_period(qs, period, lookup_prefix="article")
    qs = _filter_queryset_by_assocs(qs, lookup_prefix="article", **article_filters)

    qs = qs.values('tag').annotate(
        title=F('tag__title')
    ).annotate(
        usages=Count('*')
    ).order_by('-usages').values('title', 'usages')[:15]

    articles_in_period = _filter_queryset_by_period(Article.objects.all(), period)
    articles_in_period = _filter_queryset_by_assocs(articles_in_period, **article_filters)
    articles_in_period = articles_in_period.count()
    return pd.DataFrame.from_records(qs), articles_in_period


def tag_co_occurrence(tag: Tag, **article_filters) -> Tuple[pd.DataFrame, int]:
    qs = ArticleTag.objects.all()
    qs = _filter_queryset_by_period(qs, period=article_filters.get('period'), lookup_prefix="article")
    qs = _filter_queryset_by_assocs(qs, lookup_prefix="article", **article_filters)
    tag_usages = qs.filter(tag=tag).count()
    qs = qs.values(
        title=F("tag__title")
    ).annotate(
        cooc=Count(
            "pk", filter=Q(article__articletag__tag=tag)
        )
    ).exclude(tag=tag).order_by('-cooc')[:20]

    return pd.DataFrame.from_records(qs), tag_usages


def word_cloud_query(**filters) -> QuerySet[Article]:
    qs = _filter_article_queryset_by_tags(Article.objects.all(), filters.get('tags'))
    qs = _filter_queryset_by_period(qs, period=filters.get('period'))
    qs = _filter_queryset_by_assocs(qs, **filters)
    return qs


def csv_query(**filters) -> QuerySet[Article]:
    qs = _filter_article_queryset_by_tags(Article.objects.all(), filters.get('tags'))
    qs = _filter_queryset_by_period(qs, period=filters.get('period'))
    qs = _filter_queryset_by_assocs(qs, **filters)
    if filters.get('commented', False):
        qs = qs.filter(Exists(Comment.objects.filter(article_id=OuterRef('id'))))
    return qs
