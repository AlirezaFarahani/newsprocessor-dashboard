from http import HTTPStatus
from random import choice
from string import ascii_letters

from django.contrib.auth.models import User
from django.core.paginator import Page
from django.forms import Form
from django.template.response import TemplateResponse
from django.test import TestCase
from django.urls import reverse

from articles.models import Tag
from articles.views import BaseTagListView


# class TagDetailViewTest(TestCase):
#     def setUp(self):
#         self.user = User.objects.create_user(username='jacob', email='jacob@…')
#
#     def test_login_required_and_next_url(self):
#         response = self.client.get(reverse("articles:tag-detail", kwargs={'pk': '44'}))
#         self.assertEqual(response.status_code, 302)
#
#     def test_non_existent_tag_returns_404(self):
#         self.client.force_login(self.user)
#         response = self.client.get(reverse("articles:tag-detail", kwargs={'pk': '44'}))
#         self.assertEqual(response.status_code, HTTPStatus.NOT_FOUND)
#
#     def test_all_field_are_shown(self):
#         self.client.force_login(self.user)
#         tag_creator = User.objects.create_user(username='creator', email="c@b.com")
#         tag = Tag.objects.create(title="Quantum", creator=tag_creator)
#         response: TemplateResponse = self.client.get(reverse("articles:tag-detail", kwargs={'pk': tag.id}))
#
#         self.assertEqual(response.status_code, HTTPStatus.OK)
#         self.assertIn("tag", response.context_data)
#         self.assertEqual("Quantum", response.context_data["tag"].title)
#         self.assertEqual(tag_creator, response.context_data["tag"].creator)
#         self.assertTemplateUsed(response, 'articles/tag_detail.html')


class TagListViewTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='jacob', email='jacob@…')

    def test_login_required_and_next_url(self):
        response = self.client.get(reverse("articles:tags-all"))
        self.assertEqual(response.status_code, 302)

    def test_empty_list(self):
        self.client.force_login(self.user)
        response: TemplateResponse = self.client.get(reverse("articles:tags-all"))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(0, len(response.context_data["tag_list"]))
        self.assertEqual(1, response.context_data["page_obj"].number)
        self.assertFalse(response.context_data["page_obj"].has_next())

    # def test_empty_list_explicitly_tells_no_tag(self):
    #     pass  # TODO rendered template must tell "there's no tag". May be suggest create one?
    # for...empty tag -- https://docs.djangoproject.com/en/3.2/ref/templates/builtins/#for-empty

    def test_tags_are_loaded(self):
        self.client.force_login(self.user)
        tags = [Tag(title=f'Quantum {i}', creator=self.user) for i in range(5)]
        Tag.objects.bulk_create(tags)
        response: TemplateResponse = self.client.get(reverse("articles:tags-all"))
        self.assertEqual(response.status_code, 200)

        self.assertEqual(5, len(response.context_data["tag_list"]))
        first_tag: Tag = response.context_data["tag_list"][0]
        self.assertIsNotNone(first_tag.title)
        self.assertIsNotNone(first_tag.creator)
        self.assertTemplateUsed(response, 'articles/tag_list.html')

    def test_pagination_works(self):
        self.client.force_login(self.user)
        tags = [Tag(title=f'Quantum {i}', creator=self.user) for i in range(2 * BaseTagListView.paginate_by - 5)]
        Tag.objects.bulk_create(tags)
        response: TemplateResponse = self.client.get(reverse("articles:tags-all"))
        self.assertTrue(response.context_data["page_obj"].has_next())

        page_obj: Page = response.context_data["page_obj"]
        next_page: TemplateResponse = self.client.get(
            reverse(f"articles:tags-all", ) + f'?page={page_obj.next_page_number()}')
        self.assertEqual(next_page.status_code, 200)
        self.assertFalse(next_page.context_data["page_obj"].has_next())
        self.assertEqual(BaseTagListView.paginate_by - 5, len(next_page.context_data["tag_list"]))


# TODO: creator follows immediately
# albeit didn't work. see https://stackoverflow.com/q/10839408/1660013
# @override_settings(LANGUAGE_CODE='en-US', LANGUAGES=(('en', 'English'),))
class TagCreateViewTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='jacob', email='jacob@…')

    def test_login_required_and_next_url(self):
        self.assertEqual(self.client.get(reverse("articles:tag-create")).status_code, 302)

    def test_form_fields(self):
        self.client.force_login(self.user)
        response = self.client.get(reverse("articles:tag-create"))

        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertIn("form", response.context_data)
        self.assertIn("title", response.context_data["form"].fields)
        self.assertNotIn("creator", response.context_data["form"].fields)
        self.assertTemplateUsed(response, 'articles/tag_form.html')

    def test_valid_form_creates_tag_and_ignores_model_fields(self):
        self.client.force_login(self.user)
        fake_user = User.objects.create_user(username='fake', email="fa@ke.com")
        response: TemplateResponse = self.client.post(
            reverse("articles:tag-create"),
            # TODO: how to test model fields are ignored in these cases
            data={'title': "Quantum", 'creator_id': f"{fake_user.id}"})
        self.assertEqual(response.status_code, HTTPStatus.CREATED)

        self.assertEqual(len(Tag.objects.filter(title="Quantum")), 1)
        tag = Tag.objects.get(title="Quantum")
        self.assertEqual(tag.creator.username, 'jacob')

        form: Form = response.context_data['form']
        # only title field, test returned form is not ruined or malformed, for example by crispy
        self.assertEqual(list(form.fields.keys()), ['title', 'is_meta'])
        # form should be empty at this point
        self.assertIsNone(form['title'].data)

    def test_user_follows_his_created_tag(self):
        self.client.force_login(self.user)
        User.objects.create_user(username='fake', email="fa@ke.com")
        self.client.post(reverse("articles:tag-create"),
                         data={'title': "Quantum", 'creator_id': f"{self.user.id}"})
        self.assertEqual(1, Tag.objects.get(title='Quantum').followers.count())

    def test_duplicate_title_form_error(self):  # TODO: move form logic to test_forms.py
        self.client.force_login(self.user)
        self.client.post(reverse("articles:tag-create"), data={'title': "Quantum"})
        response: TemplateResponse = self.client.post(reverse("articles:tag-create"), data={'title': "Quantum"})
        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertTrue(hasattr(response.context_data['form'], 'data'))
        # self.assertFormError(response, 'form', 'title', "Tag with this Title already exists.")
        self.assertTrue(response.context_data['form'].has_error("title", code="unique"))

    # I don't know why, but form.is_valid() doesn't check model's Meta constraints. I had to override the form's save()
    def test_form_shows_db_constraint_errors(self):
        self.client.force_login(self.user)
        self.client.post(reverse("articles:tag-create"), data={'title': "QUANTUM"})
        # create same tag, only with lower case chars. Checked via UniqueConstraint
        response: TemplateResponse = self.client.post(reverse("articles:tag-create"), data={'title': "quantum"})
        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertTrue(hasattr(response.context_data['form'], 'data'))
        self.assertTrue(response.context_data['form'].has_error("title", code="unique"))

    def test_too_long_title_form_error(self):
        self.client.force_login(self.user)
        long_string = "".join(choice(ascii_letters) for _ in range(150))
        response: TemplateResponse = self.client.post(reverse("articles:tag-create"), data={'title': long_string})
        self.assertEqual(response.status_code, HTTPStatus.OK)
        # self.assertFormError(response, 'form', 'title', "Ensure this value has at most 24 characters (it has 150).")
        self.assertTrue(response.context_data['form'].has_error("title", code="max_length"))


class FollowTagViewTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='jacob', email='jacob@…')

    def test_login_required_is_most_precedence(self):
        url = reverse("articles:tag-toggle-followership", kwargs={'pk': 44})
        responses = [self.client.post(url), self.client.delete(url)]
        for response in responses:
            self.assertEqual(response.status_code, 302)
            self.assertRedirects(response, reverse("login") + f'?next={url}')

    def test_get_and_put_not_allowed(self):
        self.client.force_login(self.user)
        url = reverse("articles:tag-toggle-followership", kwargs={'pk': 44})
        get_response = self.client.get(url)
        put_response = self.client.put(url)
        for response in (get_response, put_response):
            self.assertEqual(HTTPStatus.METHOD_NOT_ALLOWED, response.status_code)

    def test_wrong_tag_id_returns_404(self):
        self.client.force_login(self.user)
        url = reverse("articles:tag-toggle-followership", kwargs={'pk': 44})
        responses = [self.client.post(url), self.client.delete(url)]
        for response in responses:
            self.assertEqual(response.status_code, HTTPStatus.NOT_FOUND)

    def test_following_already_followed_tag_error(self):
        tag = Tag.objects.create(title="Quantum", creator=self.user)
        tag.followers.add(self.user)
        self.client.force_login(self.user)
        url = reverse("articles:tag-toggle-followership", kwargs={'pk': tag.id})
        response = self.client.post(url)
        self.assertEqual(HTTPStatus.BAD_REQUEST, response.status_code)

    def test_unfollowing_not_followed_tag_error(self):
        tag = Tag.objects.create(title="Quantum", creator=self.user)
        self.client.force_login(self.user)
        url = reverse("articles:tag-toggle-followership", kwargs={'pk': tag.id})
        response = self.client.delete(url)
        self.assertEqual(HTTPStatus.BAD_REQUEST, response.status_code)
