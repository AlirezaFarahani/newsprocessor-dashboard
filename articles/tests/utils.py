import datetime

from articles.models import Article


# noinspection HttpUrlsUsage
def sample_article(appending_content=''):
    return Article(
        url="https://www.eurekalert.org/pub_releases/2021-06/dnl-sga062421.php",
        title="Setting gold and platinum standards where few have gone before",
        subtitle="Extreme pressure at Sandia and Lawrence Livermore national labs",
        content=r"ALBUQUERQUE, N.M. -- Like two superheroes finally joining forces, Sandia National Laboratories' Z "
                r"machine -- generator of the world's most powerful electrical pulses -- and Lawrence Livermore "
                r"National Laboratory's National Ignition Facility -- the planet's most energetic laser source -- "
                r"in a series of 10 experiments have detailed the responses of gold and platinum at pressures "
                r"so extreme that their atomic structures momentarily distorted like images in a fun-house mirror. "
                + appending_content,
        keywords=["ASTRONOMY", "SOFTWARE ENGINEERING"],
        publish_date=datetime.date.today(),
        doi="http://dx.doi.org/10.1038/s41598-021-90446-6",
        original_source="https://vtx.vt.edu/articles/2021/06/eng-bartlett-selfhealing-062021.html"
        #     Comment
        #     Associate Journal:Science Funder:National Nuclear Security Administration
    )


# noinspection HttpUrlsUsage
def sample_article2(appending_content=''):
    return Article(
        url='https://www.eurekalert.org/pub_releases/2021-07/uotb-apd070221.php',
        title="Air pollution during pregnancy may affect growth of newborn babies",
        subtitle='A study by the UPV/EHU-University of the Basque Country analyses the relationship between',
        content=r"According to studies in recent years, air pollution affects the thyroid. Thyroid hormones are "
                r"essential for regulating foetal growth and metabolism " + appending_content,
        keywords=['Pollution/Remediation', 'Medicine/Health', 'Pediatrics'],
        publish_date=datetime.date.today(),
        doi="http://dx.doi.org/10.1016/j.envres.2021.111132",
        original_source='https://www.ehu.eus/en/web/campusa-magazine/-/air-pollution-during-pregnancy-may-affect-growth-of-newborn-babies'
    )


def sample_article3(appending_content=''):
    return Article(
        url='https://google.com',
        title='A quick brown fox jumped over the lazy dog',
        content=f'lorm impsum. Lord of the Rings. ${appending_content}',
        keywords=['zoo', 'wild life', 'animals'],
        publish_date=datetime.date.today(),
        original_source='www.andisha.com'
    )
