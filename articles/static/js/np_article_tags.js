function init_tomselect() {
    const elm = document.querySelector("#np-search-tag")
    if (elm === undefined)
        return
    const articleId = document.querySelector("#np_article_id").value;
    const tomselect = new TomSelect(elm, {
        create: true,
        maxItems: 1,
        // {#maxOptions: 5, The max number of options to display in the dropdown. default=50#}
        valueField: 'id',
        labelField: 'title',
        searchField: 'title',
        /*shouldLoad: function (query) {
            return query.length >= 2;
        },*/
        load: function (query, callback) {
            const url = `/articles/${articleId}/tags/search/?q=${encodeURIComponent(query)}`;
            fetch(url)
                .then(response => response.json())
                .then(json => {
                    callback(json.items);
                }).catch(() => {
                callback();
            });
        },
        onItemAdd: function (value, item) {
            const isExistingTag = /^\d+$/.test(value.trim());
            const url = `/articles/${articleId}/tags/${isExistingTag ? value : "create-and-add"}/`;
            const context = {
                handler: function (element, responseInfo) {
                    const xhr = responseInfo.xhr;
                    const serverResponse = xhr.response;
                    const isSuccess = xhr.status >= 200 && xhr.status < 300;
                    if (isSuccess) {
                        PathDeps.refresh(`articles/${articleId}/tags/*`);
                        tomselect.clear();
                    } else if (xhr.status === HTTP_CODES.BAD_REQUEST) {
                        const errors = JSON.parse(serverResponse)
                        let errorString = ""
                        if ('title' in errors)
                            errorString = errors.title[0].message
                        else
                            errorString = Object.values(errors)[0][0].message
                        tomselect.wrapper.classList.toggle('is-invalid', true);
                        const errorPlace = htmx.find("#np-tag-validation-error");
                        errorPlace.textContent = errorString
                        htmx.removeClass(errorPlace, "invisible")
                    } else {
                        // TODO: bootstrap message, see https://htmx.org/events/#htmx:responseError
                        console.error("Error happened in creating tag")
                        console.error(serverResponse)
                    }
                },
            };
            if (!isExistingTag) {
                context.values = {title: value};
            }
            htmx.ajax('POST', url, context);
            /*const formData = new FormData();
            formData.append('title', value);
            fetch(url, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: formData,
            })
                .then(response => response.json())
                .then(() => {
                    console.log("success!!!")
                    this.clear();
                    this.close();
                })
                .catch(result => {
                    console.log(result)
                });*/
        },
        render: {
            option_create: function (data, escape) {
                return `<div class="create">Create tag <strong>${escape(data.input)}</strong>&hellip;</div>`;
            },
            option: function (data, escape) {
                return `<div class="option">${escape(data.title)}</div>`;
            },
        },
        createFilter: function (input) {
            if (input.length < 2)
                return false;
            input = input.toLowerCase();
            const titles = Object.values(this.options).map(item => item.title.toLowerCase());
            return !(titles.includes(input));
        },
        onChange: function () {
            this.wrapper.classList.toggle('is-invalid', false);
            const errorPlace = htmx.find("#np-tag-validation-error");
            errorPlace.textContent = ""
            htmx.addClass(errorPlace, "invisible")
        }
    });
}
