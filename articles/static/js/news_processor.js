const document_ready = (callback) => {
    if (document.readyState !== "loading") callback();
    else document.addEventListener("DOMContentLoaded", callback);
};

function calculate_page_height() {
    const sticky_bar = document.querySelector(".np-sticky-bar")
    if (sticky_bar)
        document.documentElement.style
            .setProperty("--sticky-bar-height", sticky_bar.offsetHeight + "px")
}

document_ready(() => {
    calculate_page_height();
})

// https://flaviocopes.com/javascript-event-delegation/
const on_delegated_click = (childSelector, eventHandler) => {
    document.addEventListener('click', eventOnElement => {
        if (eventOnElement.target.matches(childSelector)) {
            eventHandler(eventOnElement)
        }
    })
}

const HTTP_CODES = {
    OK: 200,
    CREATED: 201,
    BAD_REQUEST: 400
}

// Initializing Bootstrap toasts. It has to be done with htmx.onLoad, since they could be inserted in DOM via htmx
htmx.onLoad(() => {
    const toastElList = [].slice.call(document.querySelectorAll('.toast'))
    toastElList.map(function (toastEl) {
        const toast = new bootstrap.Toast(toastEl);
        toast.show();
        return toast;
    })
    const popoverTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="popover"]'))
    popoverTriggerList.map(function (popoverTriggerEl) {
        return new bootstrap.Popover(popoverTriggerEl);
    })
})

const toggleSubmit = function (event) {
    // const isDisabled = ![].some.call(document.querySelectorAll("input[type=text]"), function(input) {
    const form = event.target.closest('form')
    const isDisabled = !event.target.value.trim() >= 1;
    // const formNotEmpty = ![].some.call(form.querySelectorAll("input:not([type=submit]), select"),
    //     function (input) {
    //         return input.value.length;
    //     });

    const submitBtn = form.querySelector("*[type=submit]");

    if (isDisabled) {
        submitBtn.setAttribute("disabled", "disabled");
    } else {
        submitBtn.removeAttribute("disabled");
    }
};

// reset form inputs using form.reset and clearing JS enabled fields
function reset_form(form_elm) {
    // console.log(form_elm)
    form_elm.reset()
    form_elm.querySelectorAll("select + span.select2").forEach(function (node) {
        const select_elm = node.previousElementSibling;
        // console.log(select_elm);
        $(select_elm).val(null).trigger('change'); // jQuery usage :(
    })
}
