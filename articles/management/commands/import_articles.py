import argparse
import json
import logging
from json import JSONDecodeError
from typing import TextIO

from django.core.exceptions import ValidationError
from django.core.management import CommandError
from django.core.management.base import BaseCommand
from django.db import DatabaseError

from articles.operations import auto_tag_articles, save_crawled_articles


class Command(BaseCommand):
    help = 'Imports crawled news in jsonl format'
    logger = logging.getLogger('import')

    def add_arguments(self, parser):
        parser.add_argument('file', type=argparse.FileType('r'))
        parser.add_argument('-t', '--auto-tag', action='store_true',
                            help="Automatically tag articles with existing matching tags after import.")

    def import_dataset(self, dataset_file: TextIO):
        imported_articles = []
        for line_number, line in enumerate(dataset_file, 1):
            try:
                article_json = json.loads(line)
            except JSONDecodeError:
                raise CommandError(f'file is not is jsonl format. line: {line_number}')
            try:
                article = save_crawled_articles(article_json)
                if article:
                    imported_articles.append(article.id)
            except (DatabaseError, ValidationError, ValueError) as db_err:
                self.stdout.write(f'Parsing error in line {line_number}')
                self.stdout.write(repr(db_err))
        return imported_articles

    def handle(self, *args, **options):
        auto_tag = options['auto_tag']
        verbosity = options['verbosity']
        with options['file'] as export_file:
            imported_articles = self.import_dataset(export_file)

        if count := len(imported_articles):
            self.stdout.write(f'Successfully Imported {count} items')
        else:
            self.stdout.write(f'No items imported')

        if auto_tag:
            self.stdout.write('Auto tagging imported articles...')
            auto_tag_articles(imported_articles)
            self.stdout.write('Done')
