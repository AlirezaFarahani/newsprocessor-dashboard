import logging
from datetime import date, timedelta
from unittest import mock
from unittest.mock import MagicMock

from django.contrib.auth.models import User
from django.db import transaction
from django.test import TestCase

from articles.models import Tag, Article
from articles.operations import create_and_auto_apply_tag, apply_tag_on_matching_news, auto_tag_articles
from articles.tests.utils import sample_article


class TagCreationOperationTest(TestCase):
    def setUp(self) -> None:
        self.user = User.objects.create_user("Tom Hanks", email="tom@hanks.com")

    @mock.patch('articles.operations.apply_tag_on_matching_news', return_value=None, autospec=True)
    def test_tag_is_followed_by_creator(self, mocked_func: MagicMock):
        tag = Tag(title="NewTag", creator=self.user)
        saved_tag = create_and_auto_apply_tag(tag)

        self.assertTrue(saved_tag in self.user.following_tags.all())

    @mock.patch('articles.operations.apply_tag_on_matching_news', return_value=None, autospec=True)
    def test_tag_without_creator_should_be_saved(self, mocked_func: MagicMock):
        tag = Tag(title="NewTag")
        self.assertIsNone(tag.id)
        with self.assertLogs("tagging", level=logging.WARNING) as cm:
            # django performs tests as a transaction. https://stackoverflow.com/q/21458387/1660013
            with transaction.atomic():
                saved_tag = create_and_auto_apply_tag(tag)
            self.assertIn("Tag creator", cm.output[0])
        self.assertIsNotNone(saved_tag.id)
        self.assertFalse(saved_tag.followers.exists())

    @mock.patch('articles.operations.apply_tag_on_matching_news', return_value=None, autospec=True)
    def test_auto_apply_tag_operation_is_called(self, mocked_func: MagicMock):
        tag = Tag.objects.create(title="NewTag", creator=self.user)
        create_and_auto_apply_tag(tag)
        mocked_func.assert_called_with(tag)


class AutoApplyingTagTest(TestCase):
    def setUp(self) -> None:
        self.user = User.objects.create_user("Tom hanks", email="tom@hanks.com")

    def test_new_tag_is_applied_to_existing_matching_articles(self):
        article1 = Article.objects.create(title="Network of Social Managers",
                                          content="Blah Blah Blah",
                                          url="https://abc.com", publish_date=date.today())
        article2 = Article.objects.create(title="Blah Blah Blah",
                                          content="Social-Networks",
                                          url="https://def.com", publish_date=date.today() - timedelta(5))
        article3 = Article.objects.create(title="Social Networks are bad",
                                          content="Some content",
                                          url="https://ghi.com", publish_date=date.today())
        article4 = Article.objects.create(title="Social_Networks are growing fast",
                                          content="Says Zuckerberg",
                                          url="https://klm.com", publish_date=date.today())
        article5 = Article.objects.create(title="Not Matching",
                                          content="Some content",
                                          url="https://nop.com", publish_date=date.today())

        apply_tag_on_matching_news(tag1 := Tag.objects.create(title="Social Network", creator=self.user))

        self.assertEqual(set(tag1.articles.all()), {article2, article3, article4})


class MetaTagTest(TestCase):

    def test_meta_tag_is_not_applied_automatically_on_creation(self):
        meta_tag = Tag.objects.create(title="meta", is_meta=True)
        article = sample_article(appending_content="meta")
        article.save()
        self.assertNotIn(meta_tag, article.tags.all())
        apply_tag_on_matching_news(meta_tag)
        self.assertNotIn(meta_tag, article.tags.all())

    def test_meta_tag_is_not_applied_automatically_on_article_import(self):
        meta_tag = Tag.objects.create(title="Meta", is_meta=True)
        non_meta_tag = Tag.objects.create(title="Delta", is_meta=False)
        article = sample_article(appending_content="meta delta")
        article.save()

        auto_tag_articles([article.id])
        self.assertNotIn(meta_tag, article.tags.all())
        self.assertIn(non_meta_tag, article.tags.all())

    def test_tags_are_not_meta_by_default(self):
        tag = Tag.objects.create(title="Anatomy")
        self.assertFalse(tag.is_meta)

# class AlternativeTitlesTest(TestCase):
#
#     def test_tags_are_auto_applied_with_alternative_titles(self):
#         pass
#
#     def test_alternative_titles_are_unique_for_all_tags(self):
#         pass
