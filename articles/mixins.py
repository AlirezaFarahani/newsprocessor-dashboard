import csv
from abc import ABC, abstractmethod
from pathlib import Path
from typing import List, Tuple

from django.contrib.postgres.aggregates import ArrayAgg
from django.contrib.postgres.search import SearchVectorField
from django.db import connection
from django.db.models import QuerySet, Value
from django.db.models.expressions import RawSQL
from django.http import HttpRequest, HttpResponse
from django.utils import formats
from django.utils.translation import gettext as _
from django.views.generic.list import MultipleObjectMixin
from matplotlib import pyplot as plt
from wordcloud import WordCloud

from articles.models import Article

FILE = Path(__file__).resolve().parent
EXTENDED_STOPWORDS = set(map(str.strip, open(FILE / 'extended_stopwords').readlines()))
# region Export


class Export:
    name = None
    name_i18n = None

    def get_name(self):
        if self.name is not None:
            return self.name
        raise NotImplementedError("name must be specified")

    def get_name_i18n(self):
        if self.name_i18n is not None:
            return self.name_i18n
        raise NotImplementedError("name_i18b must be specified")

    def serve_export(self, request: HttpRequest):
        raise NotImplementedError("serve_export must be overridden")


class CSVWriter(ABC):
    @abstractmethod
    def write_header_row(self, writer):
        pass

    @abstractmethod
    def write_rows(self, writer):
        pass


class DefaultNewsCSVWriter(CSVWriter):

    def __init__(self, queryset: QuerySet[Article]):
        # we first clear previously deferred values duo to conflicts (like in cases of journal, funder, etc)
        # then defer again irrelevant fields
        self.queryset = queryset \
            .defer(None).defer('content', 'subtitle', 'original_source') \
            .select_related('funder', 'journal', 'institution') \
            .prefetch_related('comments') \
            .annotate(tag_titles=ArrayAgg('tags__title', default=Value([])))

    # noinspection PyMethodMayBeStatic
    def write_header_row(self, writer):
        writer.writerow(['url', 'title', 'publish_date', 'doi', 'keywords', 'comments', 'tags', 'associations'])

    def write_rows(self, writer):
        for article in self.queryset:
            # noinspection PyUnresolvedReferences
            writer.writerow([
                article.url,
                article.title,
                formats.date_format(article.publish_date, "SHORT_DATE_FORMAT"),
                article.doi,
                str(article.keywords),
                "---\n".join([f'{comment.author.username}: {comment.content}' for comment in article.comments.all()]),
                ", ".join(filter(None, article.tag_titles)),  # Django returns [None] instead of [] from DB query
                str({k: v.name for k, v in article.associations.items()}) or '',
            ])


class CSVExport(Export):
    name = 'csv'
    name_i18n = 'csv'

    def __init__(self, csv_writer: CSVWriter):
        self.csv_writer = csv_writer

    def serve_export(self, request: HttpRequest):
        # if get_language() == 'fa':
        #     timestamp = date2jalali(date.today()).strftime("%d-%m-%Y")
        # else:
        #     timestamp = date.today().strftime("%Y-%b-%d")
        # file_name = f'csv_export_{timestamp}.csv'
        response_args = {'content_type': 'text/csv'}
        response = HttpResponse(**response_args)
        writer = csv.writer(response)
        self.csv_writer.write_header_row(writer)
        self.csv_writer.write_rows(writer)
        response['Content-Disposition'] = f'attachment; filename="csv export.csv";'
        response['Cache-Control'] = 'no-cache'
        return response


class ArticleWordCloud:
    def __init__(self, queryset: QuerySet[Article]):
        self.queryset = queryset

    # TODO: see change to unmanaged model see: https://stackoverflow.com/a/58451060/1660013
    # noinspection PyMethodMayBeStatic
    def get_text_repr(self) -> List[Tuple[str, int, int]]:
        with connection.cursor() as cursor:
            annotated_query = self.queryset.annotate(
                ts=RawSQL('vector_column', params=[], output_field=SearchVectorField())
            ).values('ts')
            sql, sql_params = annotated_query.query.sql_with_params()
            cursor.execute(f"""
            SELECT * FROM ts_stat($$ {sql} $$)
            ORDER BY nentry DESC, ndoc DESC, word
            LIMIT {1000 + len(EXTENDED_STOPWORDS)}
            """, sql_params)
            ts_stat = cursor.fetchall()
            filtered_stat = [stat for stat in ts_stat if stat[0] not in EXTENDED_STOPWORDS]
            return filtered_stat

    def get_visual_repr(self) -> plt.Figure:
        word_cloud = self.get_text_repr()

        visual_world_cloud = WordCloud(
            width=800, height=800, background_color='white'
        ).generate_from_frequencies({item[0]: item[2] for item in word_cloud})
        plt.figure(figsize=(8, 8), facecolor=None)
        plt.imshow(visual_world_cloud)
        plt.axis("off")
        plt.tight_layout(pad=0)

        return plt.gcf()


class TextualWordCloudExport(Export):
    name = 'word-cloud_text'
    name_i18n = _('Textual Word cloud')

    def __init__(self, word_cloud: ArticleWordCloud):
        self.word_cloud: ArticleWordCloud = word_cloud

    def serve_export(self, request: HttpRequest):
        text_word_cloud = self.word_cloud.get_text_repr()

        response_args = {'content_type': 'text/csv'}
        response = HttpResponse(**response_args)
        writer = csv.writer(response)
        writer.writerow(['term', 'doc frequency', 'term frequency'])
        writer.writerows(text_word_cloud)
        response['Content-Disposition'] = 'attachment; filename="text word-cloud.csv";'
        response['Cache-Control'] = 'no-cache'
        return response


class VisualWordCloudExport(Export):
    name = 'word-cloud_visual'
    name_i18n = _('Visual Word cloud')

    def __init__(self, word_cloud: ArticleWordCloud):
        self.word_cloud: ArticleWordCloud = word_cloud

    def serve_export(self, request: HttpRequest):
        visual_word_cloud = self.word_cloud.get_visual_repr()

        response_args = {'content_type': 'img/jpg'}
        response = HttpResponse(**response_args)
        visual_word_cloud.savefig(response, format="jpg")
        response['Content-Disposition'] = f'attachment; filename="visual word-cloud.jpg";'
        response['Cache-Control'] = 'no-cache'
        return response


class ExportMixin(MultipleObjectMixin, ABC):

    @abstractmethod
    def get_exports(self) -> List[Export]:
        pass

    # noinspection PyMethodMayBeStatic
    def should_show_export(self) -> bool:
        return True

    def get(self, request: HttpRequest, *args, **kwargs):
        if self.should_show_export() and 'export' in request.GET:
            try:
                handler = next(exp for exp in self.get_exports() if exp.name == request.GET['export'])
                return handler.serve_export(request)
            except StopIteration:
                pass
        return super().get(request, *args, **kwargs)

    def new_request_params_with_export_link(self, export: Export):
        new_params = self.request.GET.copy()
        new_params['export'] = export.get_name()
        return new_params

    def get_context_data(self, **kwargs):
        if self.should_show_export():
            kwargs['exports'] = [{
                'name': exp.name, 'name_i18n': exp.name_i18n,
                'link': f'{self.request.path}?{self.new_request_params_with_export_link(exp).urlencode()}'
            } for exp in self.get_exports()]
        return super().get_context_data(**kwargs)
# endregion
