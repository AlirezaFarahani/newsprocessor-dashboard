from http import HTTPStatus

from django.contrib.auth.forms import AuthenticationForm, PasswordChangeForm
from django.contrib.auth.models import User
from django.template.response import TemplateResponse
from django.test import TestCase
from django.urls import reverse


class LoginViewTest(TestCase):

    def test_delete_not_allowed(self):
        response = self.client.delete(reverse("login"))
        self.assertEqual(HTTPStatus.METHOD_NOT_ALLOWED, response.status_code)

    def test_unauthenticated_user_is_redirected_for_correct_url(self):
        response = self.client.get(reverse("password_change"))
        self.assertRedirects(response, reverse("login") + f'?next={reverse("password_change")}')

    # TODO: it's better to redirect all urls to /login/ for unauthenticated user.
    # https://stackoverflow.com/questions/52190308/redirect-to-login-page-on-accepting-a-garbage-url-in-django
    def test_unauthenticated_user_gets_404_for_incorrect_url(self):
        url_path = "yek/2?q=3"
        response = self.client.get(url_path)
        self.assertEqual(response.status_code, HTTPStatus.NOT_FOUND)

    def test_correct_template_used(self):
        response: TemplateResponse = self.client.get(reverse("login"))
        self.assertTemplateUsed(response, 'registration/login.html')
        self.assertIn('username', response.context_data['form'].fields)
        self.assertIn('password', response.context_data['form'].fields)

    def test_general_login_error_is_shown(self):
        response = self.client.post(reverse("login"), data={'username': 'jacob', 'password': "such security"})
        self.assertEqual(HTTPStatus.OK, response.status_code)
        form: AuthenticationForm = response.context_data['form']
        form_clean_fields = form.cleaned_data.keys()
        self.assertEqual(form_clean_fields, form_clean_fields | {'username', 'password'})
        self.assertFalse(0, len(form.non_field_errors()))

    def test_registered_user_can_login(self):
        User.objects.create_user(username="jacob", password="such security")
        response = self.client.post(reverse("login"), data={'username': 'jacob', 'password': "such security"})
        self.assertRedirects(response, reverse("home"), fetch_redirect_response=False)

    def test_logged_in__user_is_redirected_to_home(self):
        user = User.objects.create_user(username="jacob", email="a@b.com")
        self.client.force_login(user)
        response = self.client.get(reverse("login"))
        self.assertRedirects(response, reverse("home"), fetch_redirect_response=False)


class LogoutViewTest(TestCase):

    # TODO: only allow post or delete for logout. now all methods are allowed

    def test_unauthenticated_user_logout_no_problem(self):
        response = self.client.get(reverse("logout"))
        self.assertRedirects(response, reverse("login"))

    def test_authenticated_user_logouts(self):
        user = User.objects.create_user(username="jacob", email="a@b.com")
        self.client.force_login(user)
        response = self.client.get(reverse("logout"))
        self.assertRedirects(response, reverse("login"))


class ChangePasswordViewTest(TestCase):

    def test_delete_not_allowed(self):
        self.client.force_login(User.objects.create_user(username="jacob", email="a@b.com"))
        response = self.client.delete(reverse("password_change"))
        self.assertEqual(HTTPStatus.METHOD_NOT_ALLOWED, response.status_code)

    def test_only_authenticated_user(self):
        response = self.client.get(reverse("password_change"))
        self.assertRedirects(response, reverse("login") + f'?next={reverse("password_change")}')

    def test_correct_template_used(self):
        self.client.force_login(User.objects.create_user(username="jacob", email="a@b.com"))
        response: TemplateResponse = self.client.get(reverse("password_change"))
        self.assertTemplateUsed(response, 'registration/password_change_form.html')
        form_fields = response.context_data['form'].fields
        self.assertListEqual(['old_password', 'new_password1', 'new_password2'],
                             list(form_fields.keys()))

    def test_form_errors_are_shown(self):
        self.client.force_login(User.objects.create_user(username="jacob", password="such security"))
        response = self.client.post(reverse("password_change"),
                                    data={
                                        'old_password': 'such security',
                                        'new_password1': 'much security',
                                        'new_password2': 'low security',
                                    })
        form: PasswordChangeForm = response.context_data['form']
        form_clean_fields = form.cleaned_data.keys()
        self.assertEqual(form_clean_fields, form_clean_fields | {'old_password', 'new_password1'})
        self.assertListEqual(['new_password2'], list(form.errors.keys()))

    def test_password_changes_with_correct_date(self):
        # self.client.force_login(User.objects.create_user(username="jacob", password="such security"))
        user = User.objects.create_user(username="jacob", password="such security")
        old_pass = 'such security'
        new_pass = 'sarbaz e vatan'
        self.assertTrue(user.check_password(old_pass))
        self.client.post(reverse("login"), data={'username': 'jacob', 'password': 'such security'})
        response = self.client.post(reverse("password_change"),
                                    data={
                                        'old_password': 'such security',
                                        'new_password1': new_pass,
                                        'new_password2': new_pass,
                                    })
        self.assertRedirects(response, reverse("home"), fetch_redirect_response=False)
        user.refresh_from_db()
        self.assertFalse(user.check_password(old_pass))
        self.assertTrue(user.check_password(new_pass))
        # current session must kept logged-in
        home_response = self.client.get(reverse("home"), follow=True)
        self.assertEqual(HTTPStatus.OK, home_response.status_code)

        # TODO: other sessions must logout


class PasswordResetExclusionTest(TestCase):

    def test_password_reset_urls_404(self):
        responses = [
            self.client.get("/accounts/password_reset"),
            self.client.get("/accounts/password_reset_complete"),
        ]
        for response in responses:
            self.assertEqual(HTTPStatus.NOT_FOUND, response.status_code)
