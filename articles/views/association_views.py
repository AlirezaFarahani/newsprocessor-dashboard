from typing import Dict

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Field
from dal import autocomplete
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import QuerySet, Count, Exists, OuterRef
from django.db.models.functions import Length
from django.forms import ModelForm
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils.translation import gettext as _
from django.views import View
from django.views.generic import ListView, CreateView, UpdateView, DetailView

from articles.forms import AssociationSearchForm, AssociationProfileForm
from articles.models import Association, AssociationProfile, Article

AssocType = Association.AssociationType


class SearchableAssociationsListView(LoginRequiredMixin, ListView):
    model = Association
    paginate_by = 15
    template_name = 'articles/associations_list_searchable.html'

    # noinspection PyAttributeOutsideInit
    def get(self, request, *args, **kwargs):
        self.assoc_type: AssocType = AssocType[kwargs['association_type']]
        if request.GET and 'q' in request.GET:
            self.form = AssociationSearchForm(request.GET)
        else:
            self.form = AssociationSearchForm()
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        qs: QuerySet = super().get_queryset().filter(type=self.assoc_type).select_related('profile')
        if self.form.is_valid():
            qs = qs.filter(name__icontains=self.form.cleaned_data['q'])
        assoc_related_name = {
            AssocType.Journal: 'published_articles',
            AssocType.Institution: 'researched_articles',
            AssocType.Funder: 'funded_articles'
        }
        return qs.annotate(article_count=Count(assoc_related_name[self.assoc_type])).order_by('-article_count')

    def get_context_data(self, *, object_list=None, **kwargs):
        menus = {
            AssocType.Journal: 'journals', AssocType.Institution: 'institutions', AssocType.Funder: 'funders'}
        context = {
            'association_type': self.assoc_type.name,
            'active_menu': menus[self.assoc_type],
            'active_menu_i18n': _(menus[self.assoc_type].title()),
            'form': self.form,
        }
        context.update(kwargs)
        return super().get_context_data(**context)


class AssociationAutocompleteView(LoginRequiredMixin, autocomplete.Select2QuerySetView):
    paginate_by = 10
    model_field_name = 'name'

    def dispatch(self, request, *args, **kwargs):
        # noinspection PyAttributeOutsideInit
        self.association_type: AssocType = Association.AssociationType[kwargs['association_type'].title()]
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        qs = Association.objects.filter(type=self.association_type)
        outer_ref = {self.association_type.name.lower(): OuterRef('id')}
        subquery_filters = {}
        if (journal := self.forwarded.get('journal', None)) and self.association_type != AssocType.Journal:
            subquery_filters['journal'] = journal
        if (institution := self.forwarded.get('institution', None)) and self.association_type != AssocType.Institution:
            subquery_filters['institution'] = institution
        if (funder := self.forwarded.get('funder', None)) and self.association_type != AssocType.Funder:
            subquery_filters['funder'] = funder

        qs = qs.filter(Exists(Article.objects.filter(**subquery_filters).filter(**outer_ref)))

        if self.q:
            qs = qs.filter(name__icontains=self.q).order_by(Length('name'), 'name')

        return qs


class AssociationFormHelper(FormHelper):
    form_tag = False
    # form_class = 'form-horizontal'
    disable_csrf = True

    # label_class = 'col-12 col-md-3'
    # field_class = 'col-12 col-md-6'

    def __init__(self, form=None):
        super().__init__(form)
        self['icon'].wrap(Field, template='field_file_fixed.html')
        self['description'].wrap(Field, rows=6)


class AssociationProfileCreateView(LoginRequiredMixin, CreateView):
    template_name = 'articles/association_form.html'
    model = AssociationProfile
    form_class = AssociationProfileForm

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        form.helper = AssociationFormHelper(form)
        return form

    def form_valid(self, form: ModelForm):  # TODO: prevent empty form submission!!!
        assoc = get_object_or_404(Association, id=self.kwargs['pk'])
        form.instance.association = assoc
        messages.success(self.request, _("Association updated successfully"))
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        assoc = get_object_or_404(Association, id=self.kwargs['pk'])
        action_url = reverse('articles:association-profile-create', kwargs={'pk': assoc.pk})
        return super().get_context_data(association=assoc, action_url=action_url, **kwargs)


class AssociationProfileUpdateView(LoginRequiredMixin, UpdateView):
    template_name = 'articles/association_form.html'
    model = AssociationProfile
    form_class = AssociationProfileForm

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        form.helper = AssociationFormHelper(form)
        return form

    def form_valid(self, form):
        messages.success(self.request, _("Association updated successfully"))
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        action_url = reverse('articles:association-profile-update', kwargs={'pk': self.object.association.pk})
        cancel_url = reverse('articles:association-profile', kwargs={'pk': self.object.association.pk})
        return super().get_context_data(association=self.object.association,
                                        cancel_url=cancel_url, action_url=action_url, **kwargs)


class AssociationProfileDetailView(LoginRequiredMixin, DetailView):
    template_name = 'articles/association_detail.html'
    model = AssociationProfile


class AssociationProfileView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        assoc = get_object_or_404(Association, id=kwargs['pk'])
        if hasattr(assoc, 'profile'):
            return AssociationProfileDetailView.as_view()(request, *args, **kwargs)
        return HttpResponseRedirect(
            redirect_to=reverse('articles:association-profile-create', kwargs={'pk': assoc.id}))
