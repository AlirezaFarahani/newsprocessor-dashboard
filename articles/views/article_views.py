from typing import List

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.db.models import Exists, OuterRef, Case, When
from django.shortcuts import get_object_or_404
from django.utils.translation import gettext as _
from django.views.generic import DetailView
from django.views.generic.list import MultipleObjectMixin, ListView

from articles.forms import TextSearchForm
from articles.mixins import ExportMixin, TextualWordCloudExport, Export, ArticleWordCloud, VisualWordCloudExport, \
    DefaultNewsCSVWriter, CSVExport
from articles.models import Article, ArticleTag, Tag, Association, Comment
from articles.operations import advanced_article_search


class SortableListMixin(MultipleObjectMixin):
    sort_keys = []  # TODO: we need keys translation to show in <select>

    def get_ordering(self):
        # noinspection PyUnresolvedReferences
        params: QueryDict = self.request.GET
        return params.get('order_by', default=self.ordering)  # TODO

    def get_sort_keys(self):
        return self.sort_keys

    def get_context_data(self, *, object_list=None, **kwargs):
        ordering: str = self.get_ordering()
        current_sort_order, current_sort_key = \
            ('-', ordering[1:]) if ordering.startswith('-') else ('', ordering)
        context = {
            'sortable': True,
            'available_sort_keys': self.get_sort_keys(),
            'sort_key': current_sort_key,
            'sort_order': current_sort_order
        }
        context.update(kwargs)
        return super().get_context_data(object_list=object_list, **context)


# region Article
class SortableArticleListView(SortableListMixin, ListView):
    model = Article
    paginate_by = Article.default_pagination
    ordering = "-publish_date"
    sort_keys = [("publish_date", _("Publish date")), ("title", _("Title"))]

    def get_queryset(self):
        return super().get_queryset() \
            .defer('url', 'subtitle', 'doi', 'keywords', 'original_source', 'journal', 'institution', 'funder') \
            .prefetch_related('tags') \
            .annotate(has_comments=Exists(Comment.objects.filter(article=OuterRef('pk'))))

    def get_exports(self) -> List[Export]:
        return [CSVExport(DefaultNewsCSVWriter(self.get_queryset())),
                TextualWordCloudExport(ArticleWordCloud(self.get_queryset())),
                VisualWordCloudExport(ArticleWordCloud(self.get_queryset()))]


class AllArticlesListView(LoginRequiredMixin, ExportMixin, SortableArticleListView):
    extra_context = {'active_menu': 'all_articles'}
    template_name = 'articles/base_article_list.html'

    def get_exports(self) -> List[Export]:
        return [TextualWordCloudExport(ArticleWordCloud(self.get_queryset())),
                VisualWordCloudExport(ArticleWordCloud(self.get_queryset()))]


class FollowedArticlesListView(LoginRequiredMixin, ExportMixin, SortableArticleListView):
    extra_context = {'active_menu': 'interested_articles'}
    template_name = 'articles/base_article_list.html'

    def get_queryset(self):
        user: User = self.request.user
        # Distinct is bad, Counting Distinct is abysmal! We could limit distinct on minimum necessary fields to decrease
        # query time but it's still bad.
        # However, since we just to know there exist a followed tag on the article, using EXISTS operator we can avoid
        # DISTINCT altogether and dramatically decrease both queries time, (specially main query for getting articles)
        return super().get_queryset().filter(Exists(ArticleTag.objects
                                                    .filter(article=OuterRef('pk'))
                                                    .filter(tag__followers=user)
                                                    .values('pk')))
        # followed_tags = user.following_tags.all()
        # return super().get_queryset().filter(tags__id__in=followed_tags) \
        #     .distinct('id', *map(lambda x: x[0], self.sort_keys))
        # .distinct()

    def get_exports(self) -> List[Export]:
        return [CSVExport(DefaultNewsCSVWriter(self.get_queryset())),
                TextualWordCloudExport(ArticleWordCloud(self.get_queryset())),
                VisualWordCloudExport(ArticleWordCloud(self.get_queryset()))]


class CommentedArticlesListView(LoginRequiredMixin, ExportMixin, SortableArticleListView):
    extra_context = {'active_menu': 'commented_articles'}
    template_name = 'articles/base_article_list.html'

    def get_queryset(self):
        return super().get_queryset().filter(Exists(Comment.objects.filter(article=OuterRef('pk'))))

    def get_exports(self) -> List[Export]:
        return [CSVExport(DefaultNewsCSVWriter(self.get_queryset())),
                TextualWordCloudExport(ArticleWordCloud(self.get_queryset())),
                VisualWordCloudExport(ArticleWordCloud(self.get_queryset()))]


class CommentedByMeArticlesListView(LoginRequiredMixin, ExportMixin, SortableArticleListView):
    extra_context = {'active_menu': 'commented_by_me_articles'}
    template_name = 'articles/base_article_list.html'

    def get_queryset(self):
        user: User = self.request.user
        return super().get_queryset().filter(comments__author=user)

    def get_exports(self) -> List[Export]:
        return [CSVExport(DefaultNewsCSVWriter(self.get_queryset())),
                TextualWordCloudExport(ArticleWordCloud(self.get_queryset())),
                VisualWordCloudExport(ArticleWordCloud(self.get_queryset()))]


class ArticlesByTagListView(LoginRequiredMixin, ExportMixin, SortableArticleListView):
    template_name = 'articles/articles_by_tag.html'
    model = Article
    paginate_by = Article.default_pagination

    def get_queryset(self):
        tag = get_object_or_404(Tag, pk=self.kwargs['tag_id'])
        return super().get_queryset().filter(tags=tag)

    def get_exports(self) -> List[Export]:
        return [CSVExport(DefaultNewsCSVWriter(self.get_queryset())),
                TextualWordCloudExport(ArticleWordCloud(self.get_queryset())),
                VisualWordCloudExport(ArticleWordCloud(self.get_queryset()))]


class ArticlesByAssociationListView(LoginRequiredMixin, ExportMixin, SortableArticleListView):
    template_name = 'articles/articles_by_tag.html'
    model = Article
    paginate_by = Article.default_pagination

    def get_queryset(self):
        association = get_object_or_404(Association, pk=self.kwargs['association_id'])
        # return Article.objects.filter(association)
        return association.articles().all() & super().get_queryset()

    def get_exports(self) -> List[Export]:
        return [CSVExport(DefaultNewsCSVWriter(self.get_queryset())),
                TextualWordCloudExport(ArticleWordCloud(self.get_queryset())),
                VisualWordCloudExport(ArticleWordCloud(self.get_queryset()))]


class ArticleDetailView(LoginRequiredMixin, DetailView):
    model = Article


# endregion Articles


# region Text Search
class TextSearchArticleListView(LoginRequiredMixin, ExportMixin, ListView):
    extra_context = {'sortable': False}
    template_name = "articles/article_list_textsearch.html"
    model = Article
    paginate_by = Article.default_pagination
    # paginator_class = ArticleTextSearchPaginator
    allow_empty = True
    _keys = ('q', 'journal', 'institution', 'funder')

    # def get_paginator(self, queryset, per_page, orphans=0,
    #                   allow_empty_first_page=True, **kwargs):
    #     return self.paginator_class(self.queryset.query,
    #                                 per_page, orphans=0, allow_empty_first_page=True)

    def get(self, request, *args, **kwargs):
        if any(key in self._keys for key in self.request.GET.keys()):
            self.form = TextSearchForm(self.request.GET)
        else:
            self.form = TextSearchForm()

        if not self.form.is_valid():
            self.paginate_by = 0  # disable pagination mechanism when not searching
        return super().get(request, *args, **kwargs)

    def should_show_export(self):
        return self.form.is_valid()

    def get_context_data(self, **kwargs):
        show_advance = self.form.is_valid() and any([v for k, v in self.form.cleaned_data.items() if k != 'q'])
        return super().get_context_data(form=self.form, show_advanced_filters=show_advance, **kwargs)

    def get_queryset(self):
        if len(self.request.GET) == 0 or not self.form.is_valid():
            return Article.objects.none()

        if not self.form.is_valid():
            raise ValueError("Form should be checked for validity earlier")
        data = self.form.cleaned_data
        return advanced_article_search(**data)\
            .defer('url', 'subtitle', 'doi', 'keywords', 'original_source', 'journal', 'institution', 'funder') \
            .prefetch_related('tags') \
            .annotate(has_comments=Exists(Comment.objects.filter(article=OuterRef('pk'))))

    def get_exports(self) -> List[Export]:
        return [CSVExport(DefaultNewsCSVWriter(self.get_queryset())),
                TextualWordCloudExport(ArticleWordCloud(self.get_queryset())),
                VisualWordCloudExport(ArticleWordCloud(self.get_queryset()))]
