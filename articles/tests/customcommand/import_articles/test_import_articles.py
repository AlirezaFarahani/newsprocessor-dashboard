import os
from io import StringIO

from django.core.management import call_command, CommandError
from django.test import TestCase


class ImportArticlesTest(TestCase):
    def test_input_file_required(self):
        out = StringIO()
        msg = "Error: the following arguments are required: file"
        with self.assertRaisesMessage(CommandError, msg):
            call_command('import_articles', stdout=out)

    def test_only_jsonl_is_accepted(self):
        out = StringIO()
        with self.assertRaises(CommandError) as ctx:
            call_command('import_articles', os.path.join(os.path.dirname(__file__), 'not_a_jsonl_file.xml'),
                         stdout=out)
            self.assertIn('file is not is jsonl format', ctx.exception.args[0])

    def test_dump_file_imported_successfully(self):
        out = StringIO()
        call_command('import_articles', os.path.join(os.path.dirname(__file__), 'crawled_data_correct.jsonl'),
                     stdout=out)
        self.assertIn('Successfully Imported 3 items', out.getvalue())

    def test_auto_tag_is_off_by_default(self):
        out = StringIO()
        call_command('import_articles', os.path.join(os.path.dirname(__file__), 'crawled_data_correct.jsonl'),
                     '--verbosity', '1',
                     stdout=out)
        self.assertNotIn('Auto tagging imported articles...', out.getvalue())

    def test_auto_tag_option_works(self):
        out = StringIO()
        call_command('import_articles', os.path.join(os.path.dirname(__file__), 'crawled_data_correct.jsonl'),
                     '--auto-tag',
                     stdout=out)
        output = out.getvalue()
        self.assertIn('Successfully Imported 3 items', output)
        self.assertIn('Auto tagging imported articles...\nDone', output)

    def test_corrupt_items_dont_cancel_command(self):
        out = StringIO()
        call_command('import_articles', os.path.join(os.path.dirname(__file__), 'crawled_data_corrupted.jsonl'),
                     stdout=out)

        result = out.getvalue()
        self.assertIn('No items imported', result)
        for i in range(1, 8):
            self.assertIn(f'Parsing error in line {i}', result)
