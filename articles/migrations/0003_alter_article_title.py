# Generated by Django 4.0 on 2021-12-23 19:44

import articles.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0002_alter_article_options_alter_association_options_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='title',
            field=models.CharField(max_length=255, unique=True, validators=[articles.validators.validate_non_empty_text], verbose_name='Title'),
        ),
    ]
