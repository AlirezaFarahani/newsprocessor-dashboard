#!/usr/bin/env bash
# https://stackoverflow.com/a/246128/1660013
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
source "$SCRIPT_DIR"/venv/bin/activate
python manage.py import_articles "$1"
