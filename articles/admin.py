from django.contrib import admin
from django.db import models
from django.forms import TextInput

from articles.models import Article, Tag, Association, ArticleTag, Comment, Digest, AssociationProfile
from articles.operations import simple_article_search_queryset


class AssociationProfileInline(admin.StackedInline):
    model = AssociationProfile
    view_on_site = True


class AssociationAdmin(admin.ModelAdmin):
    list_filter = ['type', ('profile', admin.EmptyFieldListFilter)]
    search_fields = ['name']
    view_on_site = False
    readonly_fields = ['type']
    inlines = [AssociationProfileInline]


class ArticleTagInline(admin.TabularInline):
    model = ArticleTag
    readonly_fields = ('added_by',)
    extra = 1
    view_on_site = False
    search_fields = ['tag__title']
    autocomplete_fields = ['tag']


class CommentInline(admin.TabularInline):
    model = Comment
    readonly_fields = ('author',)
    extra = 1
    max_num = 1


class ArticleAdmin(admin.ModelAdmin):
    list_per_page = 50
    list_filter = ['publish_date', ('comments', admin.EmptyFieldListFilter)]
    search_fields = ['title', 'content']
    show_full_result_count = True  # for now. very inefficient for >100_000x records
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size': '100'})},
    }
    inlines = [ArticleTagInline, CommentInline]
    # noinspection PyUnresolvedReferences
    autocomplete_fields = ['journal', 'institution', 'funder']
    view_on_site = True  # redirects to Model#get_absolute_url()

    def get_search_results(self, request, queryset, search_term):
        queryset, use_distinct = super().get_search_results(request, queryset, search_term)
        return simple_article_search_queryset(search_term, queryset) or queryset, use_distinct

    def save_formset(self, request, form, formset, change):
        if formset.model == ArticleTag:
            article_tags = formset.save(commit=False)
            for obj in formset.deleted_objects:
                obj.delete()
            article_tag: ArticleTag
            for article_tag in article_tags:
                article_tag.added_by = request.user
                article_tag.save()
            formset.save_m2m()
        if formset.model == Comment:
            comments = formset.save(commit=False)
            for obj in formset.deleted_objects:
                obj.delete()
            comment: Comment
            for comment in comments:
                comment.author = request.user
                comment.save()
            formset.save_m2m()
        else:
            super().save_formset(request, form, formset, change)


class TagAdmin(admin.ModelAdmin):
    readonly_fields = ('creator',)
    filter_horizontal = ('followers',)
    list_filter = ['is_meta']
    # noinspection PyUnresolvedReferences
    search_fields = ['title']
    view_on_site = False  # for now that we don't show tag details in separate page

    def save_model(self, request, obj: Tag, form, change):
        if not change:  # creating for the first time
            obj.creator = request.user
        obj.save()


class CommentAdmin(admin.ModelAdmin):
    readonly_fields = ('author',)
    autocomplete_fields = ('article',)

    def save_model(self, request, obj: Comment, form, change):
        if not change:
            obj.author = request.user
        super().save_formset(request, obj, form, change)


# class ArticleTagAdmin(admin.ModelAdmin):
#     readonly_fields = ('added_by', )
#
#     def save_model(self, request, obj: Tag, form, change):
#         if getattr(obj, 'added_by', None) is None:
#             obj.added_by = request.user
#         obj.save()
#
#     def has_module_permission(self, request):
#         return False  # Don't show this model in Admin index view.

class DigestAdmin(admin.ModelAdmin):
    filter_horizontal = ('articles',)


# admin.site.register([Article, Tag, Association])
admin.site.register(Article, ArticleAdmin)
admin.site.register(Tag, TagAdmin)
admin.site.register(Association, AssociationAdmin)
admin.site.register(Digest, DigestAdmin)
admin.site.register(Comment, CommentAdmin)
