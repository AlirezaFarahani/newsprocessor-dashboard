# Generated by Django 4.0 on 2021-12-29 11:48

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0007_alter_digest_recipient_alter_tag_title'),
    ]

    operations = [
        migrations.AddConstraint(
            model_name='article',
            constraint=models.CheckConstraint(check=models.Q(('publish_date__lte', datetime.date(2021, 12, 29))), name='article_publish_date'),
        ),
    ]
