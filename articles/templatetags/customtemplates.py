import logging
import re
from typing import Dict
from urllib.parse import urlparse, quote

from django import template
from django.db.models import Model
from django.template.defaultfilters import stringfilter
from django.utils.translation import get_language

logger = logging.getLogger("custom_templatetags")
register = template.Library()


@register.filter(name="url_path")
@stringfilter
def extract_url_path(value: str) -> str:
    return urlparse(value).path


@register.filter(name="url_encoded")
@stringfilter
def urlencode(value: str) -> str:
    return quote(value, safe='')


@register.simple_tag(takes_context=True)
def replace_param(context, **kwargs):
    d = context['request'].GET.copy()
    for k, v in kwargs.items():
        d[k] = v
    # remove keys with empty values e.g. a=''
    for k in [k for k, v in d.items() if not v]:
        del d[k]
    return d.urlencode()


# def update_page_param(context, new_page_number):
# query: QueryDict = context['request'].GET.copy()
# if 'page' in query:
#     query.pop('page')
#     query['page'] = new_page_number
# return query.urlencode()


EN_TO_FA_MAP = {
    "0": "۰",
    "1": "۱",
    "2": "۲",
    "3": "۳",
    "4": "۴",
    "5": "۵",
    "6": "۶",
    "7": "۷",
    "8": "۸",
    "9": "۹",
}
FA_TO_EN_MAP = {
    "۰": "0",
    "۱": "1",
    "۲": "2",
    "۳": "3",
    "۴": "4",
    "۵": "5",
    "۶": "6",
    "۷": "7",
    "۸": "8",
    "۹": "9",
}


def _replace(string: str, dictionary: Dict[str, str]) -> str:
    if not isinstance(string, str):
        raise TypeError("accept string type")

    pattern = re.compile("|".join(dictionary.keys()))
    return pattern.sub(lambda x: dictionary[x.group()], string)


def en_to_fa(string: str) -> str:
    return _replace(string, EN_TO_FA_MAP)


def fa_to_en(string: str) -> str:
    return _replace(string, FA_TO_EN_MAP)


@register.filter(name='l10n_nums')
@stringfilter
def localize_numbers(value: str):
    curr_lang = get_language()
    if curr_lang == 'fa':
        return en_to_fa(value)
    return value


@register.filter(name='verbose_name')
def field_verbose_name(value: Model, field_name: str) -> str:
    return value._meta.get_field(field_name).verbose_name


# for sake of quick implementation of Django DetailViews
@register.filter(name='fields_to_dict')
def model_to_dict_filter(value: Model) -> Dict:
    return {field.verbose_name: field.value_to_string(value) for field in value._meta.fields}
