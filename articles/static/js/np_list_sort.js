function toggleSortOrder() {
    let sort_order_elm = document.querySelector("#np-sort-order");
    let currently_ascending = sort_order_elm.className.includes("alt");
    sort_order_elm.className = currently_ascending ? 'bi-sort-down' : 'bi-sort-down-alt';
    sort_order_elm.setAttribute('data-sort-order', currently_ascending ? 'dsc' : 'asc');
}

function updateHiddenSortInput() {
    let key = document.querySelector("#np-sort-key").value;
    let order = document.querySelector("#np-sort-order").getAttribute('data-sort-order') === "dsc" ? '-' : '';
    console.log(order + key);
    let order_by_input = document.querySelector("input[name='order_by']");
    order_by_input.value = order + key;
    order_by_input.dispatchEvent(new Event('change'));
}
