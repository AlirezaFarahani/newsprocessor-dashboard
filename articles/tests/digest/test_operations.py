# import json
# import logging
# import os
# import random
# import string
# from datetime import date, timedelta
# from typing import Union, List
#
# from django.contrib.auth.models import User
# from django.test import TestCase
#
# from articles.management.commands.import_articles import Command
# from articles.models import Article, Tag, Digest
# from articles.operations import auto_tag_imported_articles, create_digest
# from articles.tests.utils import sample_article
#
# TESTDATA_FILENAME = os.path.join(os.path.dirname(__file__), 'eurekalert_objects_test.jsonl')
#
#
# class ImportArticleTest(TestCase):
#     def setUp(self) -> None:
#         self.article = {
#             "url": "https://www.eurekalert.org/pub_releases/2021-07/uoc--uce070221.php",
#             "title": "Unusual currents explain mysterious red crab strandings",
#             "content": "For decades, people have wondered why pelagic red crabs...",
#             "date": {"$date": "2021-07-02T00:00:00.000Z"},
#             # -----------------------------------------------
#             "subtitle": "New findings suggest that abnormal ocean currents cause the occasional appearance of pelagic red crabs outside their native range",
#             "institution": "University of California - Santa Cruz",
#             "journal": "Limnology and Oceanography",
#             "funder": "Australia and Pacific Science",
#             "keywords": ["Biology", "Marine/Freshwater Biology", "Oceanography", "Earth Science", "Climate Change"],
#             "original_source_url": "https://www.global.hokudai.ac.jp/blog/cross-generational-consequences-of-lead-poisoning/",
#             "journal_article_url": "http://dx.doi.org/10.1002/lno.11870",
#         }
#
#     def test_log_error_is_written_for_non_existence_required_keys(self):
#         for k in ["title", "content", "date"]:
#             with self.assertLogs('import', level=logging.ERROR) as cm:
#                 article_copy = self.article.copy()
#                 article_copy.pop(k)
#                 Command(save_article(article_copy)
#             self.assertIn(k, "\n".join(cm.output))
#
#     def test_all_fields_are_saved_correctly(self):
#         self.assertEqual(Article.objects.count(), 0)
#         save_article(self.article)
#         self.assertEqual(Article.objects.count(), 1)
#         saved_article: Article = Article.objects.first()
#         self.assertEqual(saved_article.url, self.article['url'])
#         self.assertEqual(saved_article.title, self.article['title'])
#         self.assertEqual(saved_article.content, self.article['content'])
#         self.assertEqual(saved_article.subtitle, self.article['subtitle'])
#         self.assertEqual(saved_article.institution.name, self.article['institution'])
#         self.assertEqual(saved_article.journal.name, self.article['journal'])
#         self.assertEqual(saved_article.funder.name, self.article['funder'])
#         self.assertEqual(saved_article.original_source, self.article['original_source_url'])
#         self.assertEqual(saved_article.doi, self.article['journal_article_url'])
#         self.assertEqual(saved_article.publish_date.year, 2021)
#         self.assertEqual(saved_article.publish_date.month, 7)
#         self.assertEqual(saved_article.publish_date.day, 2)
#
#     def test_optional_keys_dont_make_problem(self):
#         for key in ['subtitle', 'institution', 'journal', 'funder', 'original_source_url', 'journal_article_url']:
#             self.article.pop(key)
#         self.assertNotIn('subtitle', self.article)
#         self.assertNotIn('funder', self.article)
#
#         save_article(self.article)
#         self.assertEqual(Article.objects.count(), 1)
#
#     def test_wrong_keys_are_ignored(self):
#         self.article['wrong'] = "wrong input data"
#         with self.assertNoLogs('import', level=logging.WARN):
#             save_article(self.article)
#         self.assertEqual(Article.objects.count(), 1)
#
#
# class AutoTaggingImportedArticleTest(TestCase):
#
#     def setUp(self) -> None:
#         self.user = User.objects.create_user(username="ali", email="a@b.com")
#         self.articles = [json.loads(line) for line in open(TESTDATA_FILENAME).readlines()]
#
#     def test_existing_tags_are_applied_on_imported_articles(self):
#         self.articles[0]['title'] = "Biological cancer"
#         self.articles[1]['title'] = "Covid outbreak"
#         self.articles[5]['title'] = "Covid-19 by USA"
#         self.articles[9]['title'] = "Defensive Biology"
#         for name in ['Biology', 'Covid', 'Defense']:
#             Tag.objects.create(title=name, creator=self.user)
#
#         for json_article in self.articles:
#             save_article(json_article)
#         for article in Article.objects.all():
#             article.publish_date = date.today()
#             article.save()
#
#         auto_tag_imported_articles()
#
#         article0 = Article.objects.filter(title='Biological cancer').get()
#         article1 = Article.objects.filter(title='Covid outbreak').get()
#         article5 = Article.objects.filter(title='Covid-19 by USA').get()
#         article9 = Article.objects.filter(title='Defensive Biology').get()
#         self.assertEqual(set(map(lambda x: x.title, article0.tags.all())), {'Biology'})
#         self.assertEqual(set(map(lambda x: x.title, article1.tags.all())), {'Covid'})
#         self.assertEqual(set(map(lambda x: x.title, article5.tags.all())), {'Covid'})
#         self.assertEqual(set(map(lambda x: x.title, article9.tags.all())), {'Biology', 'Defense'})
#
#     def test_only_newly_imported_articles_are_tagged(self):
#         self.articles[0]['title'] = "Biological cancer"
#         self.articles[1]['title'] = "Covid outbreak"
#         Tag.objects.create(title='Biology', creator=self.user)
#         Tag.objects.create(title='Covid', creator=self.user)
#         for json_article in [self.articles[0], self.articles[1]]:
#             save_article(json_article)
#         article0: Article = Article.objects.filter(title='Biological cancer').get()
#         article1: Article = Article.objects.filter(title='Covid outbreak').get()
#         article0.publish_date = date.today() - timedelta(days=1)
#         article1.publish_date = date.today() - timedelta(days=2)  # article1 should not be tagged
#         for article in [article0, article1]:
#             article.save()
#
#         auto_tag_imported_articles(date.today() - timedelta(days=1))
#
#         self.assertEqual(article0.tags.count(), 1)
#         self.assertEqual(article1.tags.count(), 0)
#
#
# def _create_article(minus_days, content='', tags: Union[None, List[Tag]] = None, title=''):
#     if not title:
#         title = ''.join(random.choice(string.ascii_letters) for _ in range(4))
#     article = sample_article()
#     article.url = f"https://google{''.join(random.choice(string.ascii_letters) for _ in range(4))}.com"
#     article.title = title
#     article.content = content
#     article.publish_date = date.today() - timedelta(days=minus_days)
#     article.save()
#     if tags:
#         article.tags.add(*tags)
#     return article
#
#
# def _create_tag_and_follow(title, user):
#     tag = Tag.objects.create(title=title, creator=user)
#     tag.followers.add(user)
#     return tag
#
#
# # noinspection DuplicatedCode
# class UserDigestTest(TestCase):
#     def setUp(self) -> None:
#         self.user = User.objects.create_user(username='jacob', email='jacob@…')
#
#     def test_digest_with_correct_articles_is_created(self):
#         football_tag = _create_tag_and_follow('Football', self.user)
#         defense_tag = _create_tag_and_follow('defense', self.user)
#         article1 = _create_article(title='Football', minus_days=1, tags=[football_tag])
#         article2 = _create_article(title='Defensive Observation', minus_days=0, tags=[defense_tag])
#         create_digest(self.user)
#
#         digest = Digest.objects.filter(recipient=self.user).get()
#         self.assertEqual(set(article.id for article in digest.articles.all()), {article1.id, article2.id})
#
#     # for now, we only check for user followed tags
#     def test_only_one_digest_per_day_is_created(self):
#         tag = _create_tag_and_follow('Football', self.user)
#         _create_article(title='Football', minus_days=1, tags=[tag])
#         create_digest(self.user)
#
#         self.assertEqual(Digest.objects.filter(recipient=self.user).count(), 1)
#
#         create_digest(self.user)
#         self.assertEqual(Digest.objects.filter(recipient=self.user).count(), 1)
#
#     def test_digest_only_contains_last_day_published_articles(self):
#         tag = _create_tag_and_follow('Football', self.user)
#         _create_article(title='Football 120', minus_days=2, tags=[tag])
#         article2 = _create_article(title='Football analysis', minus_days=1, tags=[tag])
#
#         create_digest(self.user)
#
#         digest = Digest.objects.filter(recipient=self.user).get()
#         self.assertEqual(digest.articles.count(), 1)
#         self.assertEqual(digest.articles.first().id, article2.id)
#
#     def test_empty_digest_is_not_created(self):
#         self.assertEqual(Digest.objects.count(), 0)
#         create_digest(self.user)
#         self.assertEqual(Digest.objects.count(), 0)
#
#         _create_tag_and_follow("UnrelatedTag", self.user)
#         defense_tag = Tag.objects.create(title="Defense",
#                                          creator=User.objects.create_user(username="ali", email="ali@alavi.com"))
#         _create_article(title="Defense mechanism", minus_days=0, tags=[defense_tag])
#         create_digest(self.user)
#         self.assertEqual(Digest.objects.count(), 0)
#
#     def test_digest_does_not_contain_duplicate_articles_from_previous_digests(self):
#         football_tag = _create_tag_and_follow("Football", self.user)
#         iran_tag = _create_tag_and_follow("Iran", self.user)
#         article1 = _create_article(title="Defense mechanism", minus_days=1, tags=[football_tag])
#         article2 = _create_article(title="Iran treats", minus_days=0, tags=[iran_tag])
#         create_digest(self.user)
#
#         # simulate passing of one day. but ugh! mock date object maybe?
#         for article in Article.objects.all():
#             article.publish_date -= timedelta(1)
#             article.save()
#         for digest in Digest.objects.all():
#             digest.creation_date -= timedelta(1)
#             digest.save()
#
#         _create_article(title="Iranian technical football", minus_days=0, tags=[iran_tag])
#         create_digest(self.user)
#         digests = Digest.objects.all().order_by("-creation_date")
#         self.assertEqual(digests.count(), 2)
#         self.assertIn(article2, digests.last().articles.all())
#         self.assertNotIn(article2, digests.first().articles.all())
