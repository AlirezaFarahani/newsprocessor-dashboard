from typing import List

from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView
from django.views.generic.detail import SingleObjectMixin

from articles.mixins import Export, DefaultNewsCSVWriter, CSVExport, ExportMixin
from articles.models import Digest


class DigestListView(LoginRequiredMixin, ListView):
    model = Digest
    paginate_by = 1
    ordering = "-creation_date"

    def get_queryset(self):
        return super().get_queryset().filter(recipient=self.request.user)


class DigestDetailView(LoginRequiredMixin, SingleObjectMixin, ExportMixin, ListView):
    paginate_by = 10
    template_name = 'articles/digest_detail.html'
    extra_context = {'sortable': False}

    def get(self, request, *args, **kwargs):
        # noinspection PyAttributeOutsideInit
        self.object: Digest = self.get_object(queryset=Digest.objects.filter(recipient=request.user))
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # we explicitly set the 'digest' and 'article_list' is in the context data since both SingleObjectMixin
        # and MultipleObjectMixin put things in the context data under the value of context_object_name
        context['digest'] = self.object
        context['article_list'] = context['object_list']  # paginated in MultipleObjectMixin. Don't use self.object_list
        return context

    # only used in listview(MultipleObjectMixin), so we manually pass desired queryset when calling get_object method
    def get_queryset(self):
        queryset = self.object.articles.all()
        # if you completely override get_queryset, you lost ordering feature in ListView. So ...
        ordering = self.get_ordering()
        if ordering:
            if isinstance(ordering, str):
                ordering = (ordering,)
            queryset = queryset.order_by(*ordering)
        return queryset

    def get_exports(self) -> List[Export]:
        return [CSVExport(DefaultNewsCSVWriter(self.get_queryset()))]
