from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import PasswordChangeView
from django.contrib.messages.views import SuccessMessageMixin
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils.translation import gettext as _
from django.views import View


class HomeView(LoginRequiredMixin, View):
    @staticmethod
    def get(request, *args, **kwargs):
        return HttpResponseRedirect(reverse("articles:articles-all"))


class CustomPasswordChangeView(SuccessMessageMixin, PasswordChangeView):
    success_message = _('Your password successfully updated')
