from django.contrib.auth import views as auth_views

from articles.forms import LoginForm, CustomPasswordChangeForm
from articles.views import HomeView, CustomPasswordChangeView

"""newsprocessor_dashboard URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.utils.translation import gettext_lazy as _
from django.urls import path, include, reverse_lazy

admin.site.site_title = _("News website")
admin.site.site_header = _("News website admin panel")

urlpatterns = [
    path('__debug__/', include('debug_toolbar.urls')),
    path('admin/', admin.site.urls),
    path('', HomeView.as_view(), name='home'),

    path('accounts/', include([
        path('login/', auth_views.LoginView.as_view(
            redirect_authenticated_user=True,
            authentication_form=LoginForm,
        ), name="login"),
        path('logout/', auth_views.LogoutView.as_view(), name="logout"),
        path('password_change/', CustomPasswordChangeView.as_view(
            success_url=reverse_lazy("home"),
            form_class=CustomPasswordChangeForm,
        ), name="password_change"),
    ])),

    path('', include('articles.urls')),
]
