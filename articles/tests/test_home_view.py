from http import HTTPStatus

from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse


# noinspection DuplicatedCode
class HomeViewTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='jacob', email='jacob@…')
        self.url = reverse("home")

    def test_login_required_is_most_precedence(self):
        responses = [self.client.get(self.url), self.client.put(self.url),
                     self.client.post(self.url), self.client.delete(self.url)]
        for response in responses:
            self.assertEqual(response.status_code, 302)
            self.assertRedirects(response, reverse("login") + f'?next={self.url}')

    def test_only_get_is_allowed(self):
        self.client.force_login(self.user)
        responses = [self.client.put(self.url), self.client.post(self.url), self.client.delete(self.url)]
        for response in responses:
            self.assertEqual(HTTPStatus.METHOD_NOT_ALLOWED, response.status_code)

    # def test_correct_template_used(self):
    #     self.client.force_login(self.user)
    #     response = self.client.get(reverse('home'))
    #     self.assertTemplateUsed(response, 'home.html')

    def test_home_redirects_to_articles_tab(self):
        self.client.force_login(self.user)
        response = self.client.get(reverse('home'))
        self.assertRedirects(response, (reverse('articles:articles-all')))
