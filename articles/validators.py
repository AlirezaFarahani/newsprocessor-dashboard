import re
from typing import Union

from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from django.utils.translation import gettext as _


def validate_json_array(value: Union[dict, list]):
    if type(value) is not list:
        raise ValidationError(_("Only json array is accepted."), code="non_json_array")


def validate_non_empty_text(value: str):
    if value is None or not value.strip():
        raise ValidationError(_("Input must contain non white spaces characters."), code="non_blank")


alphanumeric = RegexValidator(r'^[0-9a-zA-Z]*$', _('Only alphanumeric characters are allowed.'))
non_empty_english_string = RegexValidator(r'^([^\W_]+ )*[^\W_]+\Z',
                                          _('Only english chars and numbers separated by space.'),
                                          flags=re.ASCII,
                                          code="invalid_chars")
