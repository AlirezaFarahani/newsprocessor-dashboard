import datetime
from http import HTTPStatus

from django.contrib.auth.models import User
from django.core.exceptions import NON_FIELD_ERRORS
from django.template.response import TemplateResponse
from django.test import TestCase
from django.urls import reverse

from articles.models import Article, Association


# noinspection DuplicatedCode
class TextSearchArticleListViewTest(TestCase):
    def setUp(self) -> None:
        self.article1 = Article(title='title 1', content='content ', url='https://google.com',
                                publish_date=datetime.date.today())
        self.article1.save()
        self.article2 = Article(title='title 2', content='content ', url='https://bing.com',
                                publish_date=datetime.date.today())
        self.article2.save()
        self.article3 = Article(title='title 3', content='content ', url='https://mozilla.com',
                                publish_date=datetime.date.today())
        self.article3.save()

        self.url = reverse('articles:articles-text-search')
        self.user = User.objects.create_user(username='jacob', email='jacob@…')
        self.client.force_login(self.user)

    # region access
    def test_login_is_required(self):
        self.client.logout()
        responses = [self.client.get(self.url), self.client.put(self.url),
                     self.client.post(self.url), self.client.delete(self.url)]
        for response in responses:
            self.assertEqual(response.status_code, HTTPStatus.FOUND)  # 302
            self.assertRedirects(response, reverse("login") + f'?next={self.url}')

    def test_empty_query_renders_empty_form_and_list(self):
        response: TemplateResponse = self.client.get(self.url)
        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertTemplateUsed(response, template_name='articles/article_list_textsearch.html')
        # self.assertEqual(response.context_data['form']['q'].value(), '')
        form = response.context_data['form']
        for field in ('q', 'journal', 'institution', 'funder'):
            self.assertEqual(form[field].value(), None)
        self.assertEqual(len(response.context_data['article_list']), 0)

    def test_result_page_has_form_initials(self):
        Association.objects.create(name="1", type=Association.AssociationType.Journal)
        Association.objects.create(name="2", type=Association.AssociationType.Institution)
        Association.objects.create(name="3", type=Association.AssociationType.Funder)
        response: TemplateResponse = self.client.get(self.url, {'q': 'nietzsche', 'journal': '1',
                                                                'institution': '2', 'funder': '3'})
        form = response.context_data['form']
        self.assertEqual(form['q'].value(), 'nietzsche')
        self.assertEqual(form['journal'].value(), '1')
        self.assertEqual(form['institution'].value(), '2')
        self.assertEqual(form['funder'].value(), '3')

    # endregion access

    # region trivial tests
    def test_query_with_nextline_is_handled(self):
        response: TemplateResponse = self.client.get(f'{self.url}?q=ali\nreza')
        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertEqual(len(response.context_data['article_list']), 0)

    def test_exact_word_is_matched(self):
        self.article1.content += " superheroes"
        self.article1.save()
        response: TemplateResponse = self.client.get(f'{self.url}?q=superheroes')
        self.assertEqual(len(response.context_data['article_list']), 1)
        self.assertEqual(response.context_data['article_list'][0].id, self.article1.id)

    def test_irrelevant_word_not_match(self):
        response: TemplateResponse = self.client.get(f'{self.url}?q=asdklkjs2324')
        self.assertEqual(len(response.context_data['article_list']), 0)

    def test_stop_word_matches_nothing(self):
        response: TemplateResponse = self.client.get(f'{self.url}?q=of')
        self.assertEqual(len(response.context_data['article_list']), 0)

    # def test_whitespace_matches_nothing(self):  # we prevent submitting blank queries from UI
    #     response: TemplateResponse = self.client.get(f'{self.url}?q= \n')
    #     self.assertEqual(len(response.context_data['article_list']), 0)

    def test_empty_form_shows_form_error(self):
        response: TemplateResponse = self.client.get(f'{self.url}?q= \n&institution=')
        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertTrue(response.context_data['form'].is_bound)
        self.assertTrue(response.context_data['form'].has_error(NON_FIELD_ERRORS, code="Empty Form"))

    def test_all_related_results_are_matched(self):
        self.article1.content += " Lawrence"
        self.article1.save()
        self.article3.content = "Lawrence " + self.article3.content
        self.article3.save()
        response: TemplateResponse = self.client.get(f'{self.url}?q=Lawrence')
        result_ids = set(map(lambda a: a.id, response.context_data['article_list']))
        self.assertEqual(len(result_ids), 2)
        self.assertEqual({self.article1.id, self.article3.id}, result_ids)

    def test_search_is_case_insensitive(self):
        self.article1.content += " Alireza"
        self.article1.save()
        self.article2.content = "Alireza " + self.article3.content
        self.article2.save()
        response: TemplateResponse = self.client.get(f'{self.url}?q=alireza')
        result_ids = set(map(lambda a: a.id, response.context_data['article_list']))
        self.assertEqual(len(result_ids), 2)
        self.assertEqual({self.article1.id, self.article2.id}, result_ids)

    def test_search_removes_stems_from_both_query_and_document(self):
        self.article1.content += " dependent"
        self.article1.save()
        self.article2.content += " dependency"
        self.article2.save()
        response: TemplateResponse = self.client.get(f'{self.url}?q=dependencies')
        result_ids = set(map(lambda a: a.id, response.context_data['article_list']))
        self.assertEqual(len(result_ids), 2)
        self.assertEqual({self.article1.id, self.article2.id}, result_ids)

    # endregion trivial tests

    # region multiple fields
    def test_search_matches_on_both_title_and_content(self):
        self.article1.title = "nano"
        self.article1.save()
        self.article2.content = "nano"
        self.article2.save()
        self.article3.title = "tech"
        self.article3.save()
        response: TemplateResponse = self.client.get(f'{self.url}?q=nano')
        result_ids = set(map(lambda a: a.id, response.context_data['article_list']))
        self.assertEqual(len(result_ids), 2)
        self.assertEqual({self.article2.id, self.article1.id}, result_ids)

    def test_matches_in_title_are_ranked_higher_than_content(self):
        self.article1.content = "football"
        self.article1.save()
        self.article2.title = "football"
        self.article2.save()
        response: TemplateResponse = self.client.get(f'{self.url}?q=football')
        result_ids = list(map(lambda a: a.id, response.context_data['article_list']))
        self.assertEqual(len(result_ids), 2)
        self.assertEqual(self.article2.id, result_ids[0])
        self.assertEqual(self.article1.id, result_ids[1])

    # endregion multiple fields

    # region web-search operators
    def test_unquoted_query_works_like_AND_operator(self):
        self.article1.content = "Tom and Jerry are greatest foes."
        self.article1.save()
        self.article2.content = "jerry tom silvester tweety spike are foes."
        self.article2.save()
        response: TemplateResponse = self.client.get(f'{self.url}?q=tom jerry foe')
        result_ids = list(map(lambda a: a.id, response.context_data['article_list']))
        self.assertEqual([self.article1.id, self.article2.id], result_ids)

    # TODO: Actually we exact phrase should rank higher even if it's in content
    # and inexact one in title (which has higher weight)
    def test_quoted_query_matches_exact_sequence(self):
        self.article1.content = "Tom and Jerry are greatest foes"
        self.article1.save()
        self.article2.content = "jerry mouse is smaller than tom"
        self.article2.save()
        response: TemplateResponse = self.client.get(self.url, {'q': '"Tom and Jerry"'})
        result_ids = list(map(lambda a: a.id, response.context_data['article_list']))
        # for now, just check the highest rank belongs to article1
        self.assertEqual(result_ids[0], self.article1.id)

    def test_OR_operator_matches_either_words(self):
        self.article1.title = "volleyball"
        self.article1.save()
        self.article2.title = "football"
        self.article2.save()
        self.article3.title = "tennis"
        self.article3.save()
        response: TemplateResponse = self.client.get(self.url, {'q': 'football OR volleyball'})
        result_ids = list(map(lambda a: a.id, response.context_data['article_list']))
        self.assertEqual(len(result_ids), 2)
        self.assertEqual({self.article1.id, self.article2.id}, set(result_ids))

    def test_negation_is_supported(self):
        self.article1.content = "Tom and Jerry are greatest foes."
        self.article1.save()
        self.article2.title = "Tom Holland"
        self.article2.save()
        response: TemplateResponse = self.client.get(self.url, {'q': 'Tom -holland'})
        result_ids = list(map(lambda a: a.id, response.context_data['article_list']))
        self.assertEqual([self.article1.id], result_ids)

    # endregion web-search operators
    def test_advanced_search_empty_query_with_filtered_association(self):
        nature_journal = Association.objects.create(name="Nature", type=Association.AssociationType.Journal)
        self.article1.journal = nature_journal
        self.article1.save()
        response: TemplateResponse = self.client.get(self.url, {'journal': nature_journal.id})
        self.assertEqual(len(response.context_data['article_list']), 1)
        self.assertEqual(response.context_data['article_list'][0].id, self.article1.id)

    def test_advanced_search_query_with_associations(self):
        nature_journal = Association.objects.create(name="Nature", type=Association.AssociationType.Journal)
        self.article1.journal = nature_journal
        self.article2.journal = nature_journal
        self.article1.title = "When Nietzsche Wept"
        self.article1.save()
        response: TemplateResponse = self.client.get(self.url, {'q': 'nietzsche', 'journal': nature_journal.id})
        self.assertEqual(len(response.context_data['article_list']), 1)
        self.assertEqual(response.context_data['article_list'][0].id, self.article1.id)

    def test_exports_button_context_presence(self):
        # initial page load should not show export
        response: TemplateResponse = self.client.get(self.url)
        self.assertNotIn('exports', response.context_data)

        nature_journal = Association.objects.create(name="Nature", type=Association.AssociationType.Journal)
        # empty result set should not show export
        # TODO: duo to bad design (specially in should_show_export()), we can't decide for presence of export
        #  context data in View. We just hide it in template if object_list is faulty/empty.
        # response: TemplateResponse = self.client.get(self.url, {'q': 'nietzsche', 'journal': nature_journal.id})
        # self.assertNotIn('exports', response.context_data)

        # with any matched result exports buttons should be shown
        self.article1.title = "When Nietzsche Wept"
        self.article1.journal = nature_journal
        self.article1.save()
        response: TemplateResponse = self.client.get(self.url, {'q': 'nietzsche', 'journal': nature_journal.id})
        self.assertIn('exports', response.context_data)
