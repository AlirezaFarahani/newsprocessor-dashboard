from http import HTTPStatus

from django.contrib.auth.mixins import LoginRequiredMixin
from django.forms import ModelForm
from django.shortcuts import get_object_or_404
from django.template.response import TemplateResponse
from django.views.generic import DetailView, CreateView, DeleteView, UpdateView, ListView
from django.views.generic.detail import SingleObjectMixin

from articles.forms import CommentModelForm
from articles.models import Comment, Article
# https://docs.djangoproject.com/en/4.0/topics/class-based-views/mixins/#using-singleobjectmixin-with-listview
from articles.utils import get_object_or_none


class ArticleCommentsListView(LoginRequiredMixin, SingleObjectMixin, ListView):
    """
    Gets all comments except current user's comment.
    """
    template_name = 'articles/comment_list.html'
    pk_url_kwarg = 'article_id'
    paginate_by = 6

    def get(self, request, *args, **kwargs):
        # providing queryset as parameter to avoid using self.get_query_set() in get_object()
        self.object = self.get_object(queryset=Article.objects.all())
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['article'] = self.object
        context['comments'] = self.object_list
        context['user_comment'] = get_object_or_none(self.object.comments, author=self.request.user)
        return context

    def get_queryset(self):
        return self.object.comments.exclude(author=self.request.user)


class CommentDetailView(LoginRequiredMixin, DetailView):
    model = Comment

    def get_object(self, queryset=None):
        article = get_object_or_404(Article, pk=self.kwargs.get('article_id'))
        return Comment.objects.filter(author=self.request.user, article=article).first()  # To show empty comment in UI

    def get_context_data(self, **kwargs):
        if 'article' not in kwargs:
            kwargs['article'] = get_object_or_404(Article, pk=self.kwargs.get('article_id'))
        return super().get_context_data(**kwargs)


class CommentCreateView(LoginRequiredMixin, CreateView):
    form_class = CommentModelForm
    model = Comment

    def get_context_data(self, **kwargs):
        if 'article' not in kwargs:
            kwargs['article'] = get_object_or_404(Article, pk=self.kwargs.get('article_id'))
        return super().get_context_data(**kwargs)

    def form_valid(self, form: ModelForm):
        form.instance.author = self.request.user
        form.instance.article = get_object_or_404(Article, pk=self.kwargs.get('article_id'))
        # noinspection PyAttributeOutsideInit
        self.object = form.save()
        return TemplateResponse(self.request,
                                status=HTTPStatus.CREATED,
                                template='articles/comment_detail.html',
                                context=self.get_context_data())


class CommentUpdateView(LoginRequiredMixin, UpdateView):
    form_class = CommentModelForm
    model = Comment

    def get_object(self, queryset=None):
        article = get_object_or_404(Article, pk=self.kwargs.get('article_id'))
        return Comment.objects.get(author=self.request.user, article=article)

    def get_context_data(self, **kwargs):
        if 'article' not in kwargs:
            kwargs['article'] = get_object_or_404(Article, pk=self.kwargs.get('article_id'))
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        form.instance.author = self.request.user
        # noinspection PyAttributeOutsideInit
        self.object = form.save()
        return TemplateResponse(self.request,
                                template='articles/comment_detail.html',
                                context=self.get_context_data())


class CommentDeleteView(LoginRequiredMixin, DeleteView):
    model = Comment

    def get_object(self, queryset=None):
        article = get_object_or_404(Article, pk=self.kwargs.get('article_id'))
        return Comment.objects.get(author=self.request.user, article=article)

    def test_func(self) -> bool:
        return self.get_object().author == self.request.user

    def get_context_data(self, **kwargs):
        if 'article' not in kwargs:
            kwargs['article'] = get_object_or_404(Article, pk=self.kwargs.get('article_id'))
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        self.object.delete()
        # noinspection PyAttributeOutsideInit
        self.object = None  # Clear context. see SingleObjectMixin#get_context_data
        return TemplateResponse(self.request,
                                template='articles/comment_detail.html',
                                context=self.get_context_data())
