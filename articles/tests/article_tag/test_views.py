from http import HTTPStatus

from django.contrib.auth.models import User
from django.http import JsonResponse
from django.test import TestCase
from django.urls import reverse

from articles.models import Tag, ArticleTag
from articles.tests.utils import sample_article


class ArticleTagViewTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='jacob', email='jacob@…')
        # self.article = sample_article()
        # self.article.save()
        # self.tag = Tag.objects.create(title="Quantom", creator=self.user)

    def test_login_required_is_most_precedence(self):
        url = reverse("articles:article-tag-addOrRemove", kwargs={'article_id': 44, 'tag_id': 22})
        put_response = self.client.put(url)
        get_response = self.client.get(url)
        for response in (put_response, get_response):
            self.assertEqual(response.status_code, 302)
            self.assertRedirects(response, reverse("login") + f'?next={url}')

    def test_method_not_allowed(self):
        self.client.force_login(self.user)
        url = reverse("articles:article-tag-addOrRemove", kwargs={'article_id': 44, 'tag_id': 22})
        put_response = self.client.put(url)
        get_response = self.client.get(url)
        for response in (put_response, get_response):
            self.assertEqual(HTTPStatus.METHOD_NOT_ALLOWED, response.status_code)

    def test_add_tag_with_absent_tag_or_article_returns_404(self):
        self.client.force_login(self.user)
        url = reverse("articles:article-tag-addOrRemove", kwargs={'article_id': 44, 'tag_id': 22})
        post_response = self.client.post(url)
        self.assertEqual(HTTPStatus.NOT_FOUND, post_response.status_code)

    def test_delete_tag_with_absent_tag_or_article_returns_200(self):  # TODO: return 404
        self.client.force_login(self.user)
        url = reverse("articles:article-tag-addOrRemove", kwargs={'article_id': 44, 'tag_id': 22})
        del_response = self.client.delete(url)
        self.assertEqual(HTTPStatus.OK, del_response.status_code)

    def test_add_tag_to_article_works(self):
        self.client.force_login(self.user)
        tag = Tag.objects.create(title='Quantum', creator=self.user)
        article = sample_article()
        article.save()

        url = reverse("articles:article-tag-addOrRemove", kwargs={'article_id': article.id, 'tag_id': tag.id})
        post_response = self.client.post(url)
        self.assertTrue(1, len(ArticleTag.objects.filter(tag=tag, article=article)))
        self.assertEqual(HTTPStatus.OK, post_response.status_code)

    def test_remove_tag_from_article_works(self):
        self.client.force_login(self.user)
        tag = Tag.objects.create(title='Quantum', creator=self.user)
        article = sample_article()
        article.save()
        ArticleTag.objects.create(article=article, tag=tag, added_by=self.user)
        self.assertTrue(ArticleTag.objects.filter(article=article, tag=tag).exists())

        url = reverse("articles:article-tag-addOrRemove", kwargs={'article_id': article.id, 'tag_id': tag.id})
        del_response = self.client.delete(url)
        self.assertFalse(ArticleTag.objects.filter(article=article, tag=tag).exists())
        self.assertEqual(HTTPStatus.OK, del_response.status_code)

    def test_create_and_add_tag_works(self):
        self.client.force_login(self.user)
        article = sample_article()
        article.save()
        self.assertEqual(article.tags.count(), 0)
        url = reverse("articles:article-tag-create-and-add", kwargs={'article_id': article.id})
        response = self.client.post(url, data={'title': 'New Tag'})
        self.assertEqual(article.tags.count(), 1)
        self.assertEqual(HTTPStatus.CREATED, response.status_code)

    def test_create_and_add_tag_returns_json_errors_for_invalid_tag(self):
        self.client.force_login(self.user)
        article = sample_article()
        article.save()
        url = reverse("articles:article-tag-create-and-add", kwargs={'article_id': article.id})
        # validation_error_code = response.context_data['form'].errors['title'].data[0].code
        # self.assertEqual(validation_error_code, 'min_length')
        # response: TemplateResponse = self.client.post(url, data={'title': 'N'})
        # self.assertEqual(HTTPStatus.OK, response.status_code)
        # self.assertIsNotNone(response.context_data['form'].errors['title'])
        response: JsonResponse = self.client.post(url, data={'title': 'N'})
        self.assertEqual(HTTPStatus.BAD_REQUEST, response.status_code)
        self.assertIn('title', response.json())


class ArticleTagSearchViewTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='jacob', email='jacob@…')
        self.client.force_login(self.user)
        self.article = sample_article()
        self.article.save()
        self.url = reverse("articles:article-tag-search", kwargs={'article_id': self.article.id})

    def test_search_tag_returns_success(self):
        self.url += "?q=aaa"
        response = self.client.get(self.url)
        self.assertEqual(HTTPStatus.OK, response.status_code)

    def test_search_tag_returns_expected_json(self):
        self.url += "?q=aaa"
        response = self.client.get(self.url)
        self.assertJSONEqual(response.content, {'items': []})

    def test_search_tag_returns_all_matched_tags_for_zero_tagged_article(self):
        for i in range(4):
            Tag.objects.create(title=f"fake{i}")
        self.url += "?q=ak"
        json_response = self.client.get(self.url)
        self.assertEqual(len(json_response.json()['items']), 4)

    def test_search_tag_returns_limited_number_of_tags(self):
        for i in range(15):
            Tag.objects.create(title=f"fake{i}")
        self.url += "?q=ak"
        json_response = self.client.get(self.url)
        self.assertEqual(len(json_response.json()['items']), 5)

    def test_search_tag_excludes_articles_tags_from_results(self):
        # 6 tags are matched, 2 are already applied to article. 4 should return.
        tags = [Tag.objects.create(title=f"fake{i}") for i in range(0, 6)]

        self.article.tags.add(tags[1], tags[3])
        self.url += "?q=ak"
        json_response = self.client.get(self.url)
        self.assertEqual(len(json_response.json()['items']), 4)
