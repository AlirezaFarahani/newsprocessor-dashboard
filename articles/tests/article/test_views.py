from http import HTTPStatus

from django.contrib.auth.models import User
from django.template.response import TemplateResponse
from django.test import TestCase
from django.urls import reverse

from articles.models import Tag, Article, Comment, Association
from articles.tests.utils import sample_article, sample_article2


# noinspection DuplicatedCode
class InterestedArticlesListViewTest(TestCase):

    def setUp(self):
        self.user = User.objects.create_user(username='jacob', email='jacob@…')
        self.url = reverse("articles:articles-by-followed-tags")

    def test_login_required_is_most_precedence(self):
        responses = [self.client.get(self.url), self.client.put(self.url),
                     self.client.post(self.url), self.client.delete(self.url)]
        for response in responses:
            self.assertEqual(response.status_code, 302)
            self.assertRedirects(response, reverse("login") + f'?next={self.url}')

    def test_only_get_is_allowed(self):
        self.client.force_login(self.user)
        responses = [self.client.put(self.url),
                     self.client.post(self.url), self.client.delete(self.url)]
        for response in responses:
            self.assertEqual(HTTPStatus.METHOD_NOT_ALLOWED, response.status_code)

    def test_empty_list_when_no_article(self):
        self.client.force_login(self.user)
        response: TemplateResponse = self.client.get(self.url)
        self.assertEqual(HTTPStatus.OK, response.status_code)
        self.assertEqual(0, len(response.context_data['article_list']))
        self.assertTemplateUsed(response, 'articles/base_article_list.html')

    def test__articles_by_followed_tag__query_works(self):
        self.client.force_login(self.user)
        user2 = User.objects.create_user(username="ali", email="a@b.com")
        for i in [0, 1, 2]:
            t = Tag.objects.create(title=f"Quantum {i}", creator=self.user)
            t.followers.add(self.user)
        for i in [3, 4]:
            t = Tag.objects.create(title=f"Quantum {i}", creator=user2)
            t.followers.add(user2)

        article1 = sample_article()
        article2 = sample_article2()
        for a in (article1, article2):
            a.save()
        article3 = sample_article2()
        article3.title = "hehe"
        article3.url = "https://google.com"
        article3.save()
        article1.tags.add(Tag.objects.get(title="Quantum 0"), Tag.objects.get(title="Quantum 1"),
                          Tag.objects.get(title="Quantum 2"), Tag.objects.get(title="Quantum 4"))
        article2.tags.add(Tag.objects.get(title="Quantum 1"), Tag.objects.get(title="Quantum 3"))

        # TODO: this should be moved to test_models
        self.assertEqual(4, Article.objects.filter(tags__followers=self.user).count())  # 3 on article1, 1 on article2
        self.assertEqual(2, Article.objects.filter(tags__followers=self.user).distinct().count())

        response: TemplateResponse = self.client.get(self.url)
        articles = response.context_data['article_list']
        self.assertEqual(2, len(articles))  # current user follows tags 0,1,2 which are on article1 and article2
        self.assertEqual({article1, article2}, set(articles))


# noinspection DuplicatedCode
class CommentedArticlesListViewTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='jacob', email='jacob@…')
        self.url = reverse("articles:articles-commented")

    def test_login_required_is_most_precedence(self):
        responses = [self.client.get(self.url), self.client.put(self.url),
                     self.client.post(self.url), self.client.delete(self.url)]
        for response in responses:
            self.assertEqual(response.status_code, 302)
            self.assertRedirects(response, reverse("login") + f'?next={self.url}')

    def test_only_get_is_allowed(self):
        self.client.force_login(self.user)
        responses = [self.client.put(self.url),
                     self.client.post(self.url), self.client.delete(self.url)]
        for response in responses:
            self.assertEqual(HTTPStatus.METHOD_NOT_ALLOWED, response.status_code)

    def test_empty_list_when_no_article(self):
        self.client.force_login(self.user)
        response: TemplateResponse = self.client.get(self.url)
        self.assertEqual(HTTPStatus.OK, response.status_code)
        self.assertEqual(0, len(response.context_data['article_list']))
        self.assertTemplateUsed(response, 'articles/base_article_list.html')

    def test_distinct_list_of_articles_is_returned(self):
        self.client.force_login(self.user)
        user2 = User.objects.create_user(username="ali", email="a@b.com")

        article1 = sample_article()
        article2 = sample_article2()
        for a in (article1, article2):
            a.save()
        article3 = sample_article2()
        article3.title = "hehe"
        article3.url = "https://google.com"
        article3.save()

        Comment.objects.create(content=f"Excellent! 1", author=self.user, article=article1)
        Comment.objects.create(content=f"Excellent! 2", author=user2, article=article1)
        Comment.objects.create(content=f"Excellent! 3", author=self.user, article=article2)
        Comment.objects.create(content=f"Excellent! 4", author=user2, article=article3)
        Comment.objects.create(content=f"Excellent! 5", author=user2, article=article2)

        response: TemplateResponse = self.client.get(self.url)
        response_articles = response.context_data['article_list']
        self.assertEqual(3, len({a.id for a in response_articles}))


# noinspection DuplicatedCode
class CommentedByMeArticlesListViewTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='jacob', email='jacob@…')
        self.url = reverse("articles:articles-commented-by-me")

    def test_only_my_comments_are_returned(self):
        self.client.force_login(self.user)
        user2 = User.objects.create_user(username="ali", email="a@b.com")

        article1 = sample_article()
        article2 = sample_article2()
        for a in (article1, article2):
            a.save()
        article3 = sample_article2()
        article3.title = "hehe"
        article3.url = "https://google.com"
        article3.save()

        Comment.objects.create(content=f"Excellent! 1", author=self.user, article=article1)
        Comment.objects.create(content=f"Excellent! 2", author=user2, article=article1)
        Comment.objects.create(content=f"Excellent! 3", author=self.user, article=article2)
        Comment.objects.create(content=f"Excellent! 4", author=user2, article=article3)
        Comment.objects.create(content=f"Excellent! 5", author=user2, article=article2)

        response: TemplateResponse = self.client.get(self.url)
        response_articles = response.context_data['article_list']
        self.assertEqual({article1, article2}, set(response_articles))


# noinspection DuplicatedCode
class ArticleDetailViewTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='jacob', email='jacob@…')
        self.absent_article_url = reverse("articles:article-detail", kwargs={'pk': '44'})

    def test_login_required_is_most_precedence(self):
        responses = [self.client.get(self.absent_article_url), self.client.put(self.absent_article_url),
                     self.client.post(self.absent_article_url), self.client.delete(self.absent_article_url)]
        for response in responses:
            self.assertEqual(response.status_code, 302)
            self.assertRedirects(response, reverse("login") + f'?next={self.absent_article_url}')

    def test_only_get_is_allowed(self):
        self.client.force_login(self.user)
        responses = [self.client.put(self.absent_article_url),
                     self.client.post(self.absent_article_url), self.client.delete(self.absent_article_url)]
        for response in responses:
            self.assertEqual(HTTPStatus.METHOD_NOT_ALLOWED, response.status_code)

    def test_absent_articles_returns_404(self):
        self.client.force_login(self.user)
        response = self.client.get(self.absent_article_url)
        self.assertEqual(response.status_code, HTTPStatus.NOT_FOUND)

    def test_correct_template_used(self):
        self.client.force_login(self.user)
        article = sample_article()
        article.save()
        response = self.client.get(reverse("articles:article-detail", kwargs={'pk': article.id}))
        self.assertTemplateUsed(response, 'articles/article_detail.html')

    def test_url_title_subtitle_content_are_shown(self):
        self.client.force_login(self.user)
        article = sample_article()
        article.save()
        response: TemplateResponse = self.client.get(reverse("articles:article-detail", kwargs={'pk': article.id}))
        context = response.context_data['article'].__dict__
        for field in ['url', 'title', 'subtitle', 'content', 'keywords', 'doi', 'original_source', 'publish_date']:
            self.assertIn(field, context)
            self.assertIsNotNone(context[field])

    def test_date_tags_keywords_doi_originalsource_associations_are_shown(self):
        self.client.force_login(self.user)
        article = sample_article()
        article.journal = Association.objects.create(name="Nature", type=Association.AssociationType.Journal)
        article.save()
        Tag.objects.create(title="Temp Tag")
        article.tags.add(Tag.objects.create(title="Qunatom"))
        article.save()
        response: TemplateResponse = self.client.get(reverse("articles:article-detail", kwargs={'pk': article.id}))
        context = response.context_data['article']
        self.assertEqual(context.tags.count(), 1)
        self.assertEqual(len(context.associations), 1)
