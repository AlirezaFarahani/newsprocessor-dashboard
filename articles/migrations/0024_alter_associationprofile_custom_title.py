# Generated by Django 4.0 on 2022-06-02 07:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0023_alter_associationprofile_custom_title'),
    ]

    operations = [
        migrations.AlterField(
            model_name='associationprofile',
            name='custom_title',
            field=models.CharField(max_length=255, verbose_name='Persian title'),
        ),
    ]
